<p class="mt-8 text-center text-xs text-80">
    <a href="https://luceballoons.co.uk" class="text-primary dim no-underline">Luce Balloons</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Element Seven Digital Limited
    <span class="px-1">&middot;</span>
    v{{ \Laravel\Nova\Nova::version() }}
</p>
