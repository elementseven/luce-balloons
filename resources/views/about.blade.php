@php
$page = 'About';
$pagetitle = 'About Luce Balloons | Balloons for parties, weddings, corporate events & special occasions';
$metadescription = 'Luce Balloons is a professional balloon company that started trading in 1997. We started out as a home based business and have steadily grown into one of the premier balloon companies in Northern Ireland.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid page-top position-relative py-5 overflow-x-hidden">
	<img src="/img/shapes/circle-light-blue.svg" class="circle-red" alt="Luce balooons red circle"/>
	<div class="row py-5 mob-py-0">
		<div class="container py-5">
		  <div class="row pt-5">
		    <div class="col-lg-10 text-center text-lg-left">
		      <h1 class="mb-4 page-top text-primary mb-4"><span class="smaller">The balloon company for</span><br><span class="larger text-red">Everything</span></h1>
		      <p class="text-larger mb-4">Luce Balloons is a professional balloon company that started trading in 1997. We started out as a home based business and have steadily grown into one of the premier balloon companies in Northern Ireland and is still family run.</p>
		      <a href="{{route('contact')}}">
		      	<div class="btn btn-red btn-icon">Contact us <i class="fa fa-chevron-right"></i></div>
		      </a>
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-2 mb-5 pb-5">
  <div class="row">
    <div class="container mob-pb-0 mob-px-4">
      <div class="row text-center half_row">
        <div class="col-12 half_col">
          <h2 class="text-primary mb-5">Our Services</h2>
        </div>
        <div class="col-lg-4 col-md-6 zoom-link text-center mb-3 half_col">
        	<a href="{{route('services.parties')}}">
	        	<div class="services-img position-relative zoom-img">
	        		<picture> 
	              <source  srcset="/img/services/parties.webp" type="image/webp"/> 
	              <source srcset="/img/services/parties.jpg" type="image/jpeg"/> 
	              <img srcset="/img/services/parties.jpg" type="image/jpeg" alt="Balloons for parties in the UK and Ireland" />
	            </picture>
	        		<h3 class="services-title text-white z-2">Parties</h3>
	        	</div>
	        </a>
        </div>
        <div class="col-lg-4 col-md-6 zoom-link text-center mb-3 half_col">
        	<a href="{{route('services.birthdays')}}">
	        	<div class="services-img position-relative zoom-img">
	        		<picture> 
	              <source  srcset="/img/services/birthdays.webp" type="image/webp"/> 
	              <source srcset="/img/services/birthdays.jpg" type="image/jpeg"/> 
	              <img srcset="/img/services/birthdays.jpg" type="image/jpeg" alt="Balloons for birthdays in the UK and Ireland" />
	            </picture>
	        		<h3 class="services-title text-white z-2">Birthdays</h3>
	        	</div>
	        </a>
        </div>
        <div class="col-lg-4 col-md-6 zoom-link text-center mb-3 half_col">
        	<a href="{{route('services.babyEvents')}}">
	        	<div class="services-img position-relative zoom-img">
	        		<picture> 
	              <source  srcset="/img/services/baby-events.webp" type="image/webp"/> 
	              <source srcset="/img/services/baby-events.jpg" type="image/jpeg"/> 
	              <img srcset="/img/services/baby-events.jpg" type="image/jpeg" alt="Balloons for baby events in the UK and Ireland" />
	            </picture>
	        		<h3 class="services-title text-white z-2">Baby Events</h3>
	        	</div>
	        </a>
        </div>
        <div class="col-lg-4 col-md-6 zoom-link text-center mb-3 half_col">
        	<a href="{{route('services.corporate')}}">
	        	<div class="services-img position-relative zoom-img">
	        		<picture> 
	              <source  srcset="/img/services/corporate.webp" type="image/webp"/> 
	              <source srcset="/img/services/corporate.jpg" type="image/jpeg"/> 
	              <img srcset="/img/services/corporate.jpg" type="image/jpeg" alt="Balloons for corporate events in the UK and Ireland" />
	            </picture>
	        		<h3 class="services-title text-white z-2">Corporate</h3>
	        	</div>
	        </a>
        </div>
        <div class="col-lg-4 col-md-6 zoom-link text-center mb-3 half_col">
        	<a href="{{route('services.engagement')}}">
	        	<div class="services-img position-relative zoom-img">
	        		<picture> 
	              <source  srcset="/img/services/engagement.webp" type="image/webp"/> 
	              <source srcset="/img/services/engagement.jpg" type="image/jpeg"/> 
	              <img srcset="/img/services/engagement.jpg" type="image/jpeg" alt="Balloons for engagement in the UK and Ireland" />
	            </picture>
	        		<h3 class="services-title text-white z-2">Engagement</h3>
	        	</div>
	        </a>
        </div>
        <div class="col-lg-4 col-md-6 zoom-link text-center mb-3 half_col">
        	<a href="{{route('services.weddings')}}">
	        	<div class="services-img position-relative zoom-img">
	        		<picture> 
	              <source  srcset="/img/services/weddings.webp" type="image/webp"/> 
	              <source srcset="/img/services/weddings.jpg" type="image/jpeg"/> 
	              <img srcset="/img/services/weddings.jpg" type="image/jpeg" alt="Balloons for weddings in the UK and Ireland" />
	            </picture>
	        		<h3 class="services-title text-white z-2">Weddings</h3>
	        	</div>
	        </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-2">
  <div class="row">
    <div class="container mob-pb-0 mob-px-4">
      <div class="row">
        <div class="col-lg-10">
        	<h2 class="text-primary">A brief history…</h2>
					<p>We started to sell the helium balloons in town centres way back in 1997.</p>
					<p>Early in 1998 we were asked to attend our first wedding fayre at the Airport Hotel. (Thanks Mary) We had no idea how to do any of these balloon arrangements so got ourselves onto some balloon courses pretty quick and had our eyes opened to the wonderful world of balloons.</p>
					<p>July 1999 it was becoming very difficult to fit all the balloons arrangements in the car so we bought our first van. We still have it as part of our fleet today.</p>
					<p>In 2000 after the business started to take over every room in the house we moved into our very first workshop / party store up an entry in Castle Street.</p>
					<p>In 2004 we moved onto Castle Street proper with a small retail shop.</p>
					<p>2005 we bought our first Balloon Printing machine.</p>
					<p>2006 saw us break into the world of craft supplies.  This was a bad idea.  We don’t supply craft materials anymore so we can concentrate on our core business of balloons.  It’s what we’re good at.</p>
					<p>2008 saw us move to Railway Street.  Nice location but not very good for our customers or us to load lots of balloons into vehicles.  Our printing machine was also quite noisy and used to upset the neighbors.</p>
					<p>In November 2012 we opened our new bigger party store up here in Law’s Yard.  It has lots of Free parking, No Red Coats, and we can make as much noise as we like.</p>
					<p>With the new party store opening it enabled us to develop our Personalised range of Party Goods. We can add your personal messages and logos onto Balloons, Banners and Ribbon.</p>
					<p>In 2016 we got ourselves a new Silhouette Cameo to cut out vinyl graphics which is really great for putting multi-coloured logos onto balloons.</p>
          <p>In 2020 we opened our first online shop with our second, much better one in Early 2021.</p>
          <p>2021 saw us partner with Balloon Gas NI.  This allows us to supply helium in small cylinders for small DIY Events. The cylinders are environmental friendly as they can be refilled 1000s of times.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-2 mt-5 py-5">
  <div class="row">
    <div class="container mob-pb-0 mob-px-4">
      <div class="row text-center justify-content-center">
        <div class="col-12">
          <h2 class="text-primary mb-5">Our Team</h2>
        </div>
        <div class="col-lg-4 col-md-6 mb-4">
        	<div class="rounded-image position-relative shadow mb-3">
        		<picture> 
              <source  srcset="/img/team/eric.webp" type="image/webp"/> 
              <source srcset="/img/team/eric.jpg" type="image/jpeg"/> 
              <img srcset="/img/team/eric.jpg" type="image/jpeg" alt="Eric Patton - Luce Balloons" class="w-100" />
            </picture>
        	</div>
        	<h3 class="services-title text-primary z-2 mb-0"><b>Eric Patton</b></h3>
        	<p class="text-small mb-2"><b>Co-Owner, Balloonist and Delivery Guy</b></p>
        	<p class="text-small">I lead the team that carries out the deliveries. I also create the Mega Balloon displays at venues.</p>
        </div>
        <div class="col-lg-4 col-md-6 mb-4">
        	<div class="rounded-image position-relative shadow mb-3">
        		<picture> 
              <source  srcset="/img/team/fiona.webp" type="image/webp"/> 
              <source srcset="/img/team/fiona.jpg" type="image/jpeg"/> 
              <img srcset="/img/team/fiona.jpg" type="image/jpeg" alt="Fiona Patton - Luce Balloons" class="w-100" />
            </picture>
        	</div>
        	<h3 class="services-title text-primary z-2 mb-0"><b>Fiona Patton</b></h3>
        	<p class="text-small mb-2"><b>Co-Owner & Balloon Artiste</b></p>
        	<p class="text-small">I look after the shop based team that creates all the beautiful balloon displays that Eric’s team delivers.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection