<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <title>{{$pagetitle}}</title>

  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Luce Balloons">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:description" content="{{$metadescription}}">
  <meta name="description" content="{{$metadescription}}">
  <link href="{{ mix('css/app.css') }}" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "+442892673718",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>

</head>
<body class="front">
  @yield('fbroot')
  <div id="main-wrapper">
    <div id="app" class="front {{$pagetype}}">
      <div id="menu_btn" class="menu_btn float-left d-lg-none"><div class="nav-icon"><span></span><span></span><span></span></div></div>
      <div id="main-menu" class="menu">
        <div id="main-menu-top" class="container-fluid px-5 ipad-px-3 mob-px-1 bg-primary py-1 mob-py-1 overflow-hidden text-white d-none d-lg-block">
          <div class="row">
            <div class="col-lg-7">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="mailto:eric@luceballoons.co.uk" class="text-white"><i class="fa fa-envelope mr-1"></i> eric@luceballoons.co.uk</a>
                <a href="tel:004402892673718" class="text-white ml-4"><i class="fa fa-phone mr-1"></i> +44 28 9267 3718</a>
              </p>
            </div>
            <div class="col-lg-5 text-center text-lg-right mob-px-0 d-none d-lg-block">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="/dashboard" class="text-white d-none d-lg-inline mr-4"><i class="fa fa-user mr-1"></i>@if(Auth::check()) Customer Dashboard @else Customer Login @endif</a>
                @if(Auth::check())<a href="/logout" class="text-white d-none d-lg-inline"><i class="fa fa-sign-out mr-1"></i> Logout</a>@endif
                <a href="https://www.facebook.com/Luce.Balloons/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-facebook ml-4"></i></a>
                <a href="https://www.instagram.com/luceballoons/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-instagram ml-3"></i></a>
                <a href="https://twitter.com/Luceballoons" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
                <a href="https://www.linkedin.com/company/luce-balloons" class="text-white d-none d-lg-inline"><i class="fa fa-linkedin ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
        <div class="container-fluid px-5 ipad-px-3 mob-px-3">
          <div class="row">
            <div class="col-lg-2 mob-mt-2 py-2">
              <a href="/">
                @if($pagetype == 'light')
                <img src="/img/logos/logo.svg" class="menu_logo" alt="Luce Balloons Logo" width="98" height="80" />
                @else
                <img src="/img/logos/logo-white.svg" class="menu_logo" alt="Luce Balloons Logo" width="98" height="80"  />
                @endif
              </a>
            </div>
            @if($page != '404')
            <div class="col-lg-10 text-right d-none d-lg-block pt-4 mt-3">
              <div class="menu-links d-inline-block">
                <a href="{{route('shop')}}" class="menu-item cursor-pointer">Shop</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('custom-balloons')}}" class="menu-item cursor-pointer">Custom Balloons</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('event-services')}}" class="menu-item cursor-pointer">Event Services</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('about')}}" class="menu-item cursor-pointer">About</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('blog')}}" class="menu-item cursor-pointer">Blog</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('gallery')}}" class="menu-item cursor-pointer">Gallery</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('contact')}}" class="menu-item cursor-pointer">Contact</a>
              </div>
              <div class="menu-links d-inline-block ml-4">
                <a href="{{route('basket')}}">
                  <span class="basket-count"><i class="fa fa-shopping-cart"></i><span>{{count(Cart::content())}}</span></span>
                </a>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
      <div id="scroll-menu" class="menu d-none d-lg-block">
        <div id="main-menu-top" class="container-fluid px-5 ipad-px-3 mob-px-1 bg-primary py-1 mob-py-1 overflow-hidden text-white d-none d-lg-block">
          <div class="row">
            <div class="col-lg-9">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="mailto:eric@luceballoons.co.uk" class="text-white"><i class="fa fa-envelope mr-1"></i> eric@luceballoons.co.uk</a>
                <a href="tel:00442892673718" class="text-white ml-4"><i class="fa fa-phone mr-1"></i> +44 28 9267 3718</a>
              </p>
            </div>
            <div class="col-lg-3 text-center text-lg-right mob-px-0 d-none d-lg-block">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="https://www.facebook.com/Luce.Balloons/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-facebook ml-4"></i></a>
                <a href="https://www.instagram.com/luceballoons/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-instagram ml-3"></i></a>
                <a href="https://twitter.com/Luceballoons" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
                <a href="https://www.linkedin.com/company/luce-balloons" class="text-white d-none d-lg-inline"><i class="fa fa-linkedin ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
        <div class="container-fluid px-5 ipad-px-3 ">
          <div class="row">
            <div class="col-4 col-lg-2 pb-3 pt-2">
              <a href="/">
                <img src="/img/logos/logo.svg" class="menu_logo" alt="Luce Balloons Logo" width="98" height="80" />
              </a>
            </div>
            <div class="col-8 col-lg-10 text-right pt-4 mt-3 d-none d-lg-block">
              <div class="menu-links d-inline-block">
                <a href="{{route('shop')}}" class="menu-item cursor-pointer">Shop</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('custom-balloons')}}" class="menu-item cursor-pointer">Custom Balloons</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('event-services')}}" class="menu-item cursor-pointer">Event Services</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('about')}}" class="menu-item cursor-pointer">About</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('blog')}}" class="menu-item cursor-pointer">Blog</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('gallery')}}" class="menu-item cursor-pointer">Gallery</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('contact')}}" class="menu-item cursor-pointer">Contact</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('basket')}}" class="menu-item cursor-pointer ml-3">
                  <span class="basket-count"><i class="fa fa-shopping-cart"></i><span>{{count(Cart::content())}}</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="mobile-menu" class="mobile-menu">
        <div class="container-fluid px-3">
         <div class="row">
          <a href="/">
            <img src="/img/logos/logo.svg" class="menu_logo" width="60" height="49" alt="Luce Balloons Logo"/>
          </a>
          <div class="col-lg-10 pt-5 mt-5">
            <div class="menu-links d-block ">
              <p class="mb-0"><a href="{{route('shop')}}" class="menu-item">Shop</a></p>
              <p class="mb-0"><a href="{{route('custom-balloons')}}" class="menu-item">Custom Balloons</a></p>
              <p class="mb-0"><a href="{{route('event-services')}}" class="menu-item">Event Services</a></p>
              <p class="mb-0"><a href="{{route('about')}}" class="menu-item">About</a></p>
              <p class="mb-0"><a href="{{route('blog')}}" class="menu-item">Blog</a></p>
              <p class="mb-0"><a href="{{route('gallery')}}" class="menu-item">Gallery</a></p>
              <p class="mb-0"><a href="{{route('contact')}}" class="menu-item">Contact</a></p>
              <hr/>
              <p class="mb-2 text-smaller"><a href="/dashboard" class=""><i class="fa fa-user mr-2"></i>@if(Auth::check()) Customer Dashboard @else Customer Login @endif</a></p>
              @if(Auth::check())<p class="mb-4 text-smaller"><a href="/logout" class=""><i class="fa fa-sign-out mr-2"></i> Logout</a></p>@endif
              <p class="mb-0 text-large mt-3">
                <a href="https://www.facebook.com/Luce.Balloons"><i class="fa fa-facebook"></i></a>
                <a href="https://www.instagram.com/luceballoons"><i class="fa fa-instagram ml-3"></i></a>
                <a href="https://twitter.com/Luceballoons" ><i class="fa fa-twitter ml-3"></i></a>
                <a href="https://www.linkedin.com/company/luce-balloons" ><i class="fa fa-linkedin ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @yield('header')
    <main id="content" style="z-index: 2;">
      <div id="menu-trigger"></div>
      @yield('content')
    </main>
    <footer class="container-fluid text-center text-lg-left position-relative z-1 bg-primary">
      <div class="position-relative footer-inner row py-5 mob-mt-0 bg-primary">
        <div class="container pt-5">
          <div class="row">
            <div class="col-lg-3 mb-3 pr-5 mob-px-3 ipadp-px-3 text-center">
              <img src="/img/logos/logo-white.svg" alt="Luce Balloons logo" class="mb-4" width="166" height="135" />
              <p class="mb-2">
                <a href="https://www.facebook.com/Luce.Balloons"><i class="fa fa-facebook-square"></i></a>
                <a href="https://www.instagram.com/luceballoons"><i class="fa fa-instagram ml-2"></i></a>
                <a href="https://twitter.com/Luceballoons"><i class="fa fa-twitter ml-2"></i></a>
                <a href="https://www.linkedin.com/company/luce-balloons" ><i class="fa fa-linkedin ml-2"></i></a>
              </p>
              <p class="mb-0 d-lg-none"><a href="{{route('tandcs')}}">Terms & Conditions</a></p>
              <p class="mb-0 d-lg-none"><a href="/docs/PrivacyAndCookiePolicy3.0.pdf" target="_blank">Privacy Policy</a></p>
            </div>
            <div class="col-lg-5 mb-3 d-none d-lg-block">
              <div class="row pl-5">
                <div class="col-6">
                  <p class="mb-2"><a href="{{route('welcome')}}">Home</a></p>
                  <p class="mb-2"><a href="{{route('shop')}}">Shop</a></p>
                  <p class="mb-2"><a href="{{route('custom-balloons')}}">Custom Balloons</a></p>
                  <p class="mb-2"><a href="{{route('event-services')}}">Event Services</a></p>
                  <p class="mb-0"><a href="{{route('about')}}">About</a></p>
                </div>
                <div class="col-6">
                  
                  <p class="mb-2"><a href="{{route('blog')}}">Blog</a></p>
                  <p class="mb-2"><a href="{{route('gallery')}}">Gallery</a></p>
                  <p class="mb-2"><a href="{{route('contact')}}">Contact</a></p>
                  <p class="mb-2"><a href="{{route('tandcs')}}">Terms & Conditions</a></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 mb-3 text-lg-right">
              <p>Unit 12,<br>Rosevale Industrial Estate,<br> 171 Moira Rd, <br>Lisburn BT28 1RW</p>
              <p class="mb-1"><a href="tel:00442892673718">+44 28 9267 3718</a></p>
              <p><a href="mailto:eric@luceballoons.co.uk">eric@luceballoons.co.uk</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row text-center bg-primary">
        <div class="col-12">
          <img src="/img/logos/payment.svg" width="169" height="23" class="mb-3" alt="We accept mastercard, visa and other major card payment options"/>
          <p class="text-smaller">&copy;{{Carbon\Carbon::now()->format('Y')}} Luce Balloons Ltd | <a href="https://elementseven.co" target="_blank">Website by Element Seven</a></p>
        </div>
      </div>
    </footer>
    @yield('modals')
  </div>
  <div id="menu_body_hide"></div>
  <div id="loader">
    <div class="vert-mid">
      <div class="card p-5">
        <img id="loader-success" src="/img/icons/success.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
        <img id="loader-error" src="/img/icons/error.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
        <div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <div id="loader-message"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                  <p id="loader-second-text" class="mt-3 d-none cursor-pointer"><a id="loader-second-link"></a></p>
                </div>
                <div class="col-md-6">
                  @yield('loader-buttons')
                  <a id="loader-link">
                    <div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
                  </a>
                  <div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@yield('prescripts')
<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
<script>
  window.addEventListener("load", function(){
   window.cookieconsent.initialise({
     "palette": {
       "popup": {
         "background": "#1D3D8D",
         "text": "#ffffff"
       },
       "button": {
         "background": "#E73C62",
         "text": "#ffffff",
         "border-radius": "30px"
       }
     }
   })});
 </script>
 @yield('scripts')
 <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9X9J5K');</script>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9X9J5K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->
</body>
</html>