@php
$page = 'Homepage';
$pagetitle = 'Luce Balloons | Balloons for parties, weddings, corporate events & special occasions';
$metadescription = 'Balloons for corporate events, weddings, parties, special occasions, balloon releases and helium supplier based in Lisburn covering Belfast and Northern Ireland / NI';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid full-height position-relative mob-pt-0 page-top overflow-hidden">
  <img src="/img/shapes/circle-yellow.svg" class="circle-yellow" alt="Luce balooons yellow circle" width="311" height="311" />
  <img src="/img/shapes/circle-red.svg" class="circle-red" alt="Luce balooons red circle" width="836" height="836" />
  <img src="/img/shapes/circle-blue.svg" class="circle-blue" alt="Luce balooons blue circle" width="296" height="296" />
  <div class="row justify-content-center position-relative z-2">
    <div class="col-xl-9 lg-pl-5 mob-px-3 text-center text-lg-left">
      <div class="d-table w-100 full-height">
        <div class="d-table-cell align-middle w-100 full-height">
          <h1 class="page-top mb-5 text-primary ipad-mb-3"><span class="larger">Balloons</span><br/> for everything</h1>
          <div class="d-block">
            <a href="{{route('shop')}}">
              <div class="btn btn-red btn-icon mob-mb-1">Visit Shop <i class="fa fa-chevron-right"></i></div>
            </a>
            <b class="mx-3 d-none d-md-inline">or</b>
            <a href="{{route('custom-balloons')}}">
              <div class="btn btn-yellow btn-icon">Custom Balloons <i class="fa fa-chevron-right"></i></div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <img src="/img/shapes/scroll.svg" class="scroll" alt="Luce balooons scroll icon"/>
  </div>
</header> 
<picture> 
  <source  srcset="/img/shapes/luce-balloons.webp" type="image/webp"/> 
  <source srcset="/img/shapes/luce-balloons.png" type="image/png"/> 
  <img srcset="/img/shapes/luce-balloons.png" type="image/png" alt="Luce Balloons - Balloons for events in the UK and Ireland" class="top-balloons d-none d-lg-block z-2" width="640" height="622"/>
</picture>
@endsection
@section('content')
<div class="container-fluid py-5 mob-pb-0">
  <div class="row py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8 d-lg-none px-4 text-center pb-5">
          <h2 class="text-primary ipadp-mb-5">Great Balloons, for great events</h2>
        </div>
        <div class="col-lg-5">
          <div class="behind-blue d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/corporate.webp" type="image/webp"/> 
                <source srcset="/img/temp/corporate.jpg" type="image/jpeg"/> 
                <img srcset="/img/temp/corporate.jpg" type="image/jpeg" alt="Balloons for Corporate events in the UK and Ireland" width="314" height="392" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-7 pr-5 text-center text-lg-left mob-px-4 position-relative z-2">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-primary mt-4">Corporate</h2>
              <p class="text-large">Have a store or showroom opening and want to make an impact?</p>
              <a href="{{route('services.corporate')}}">
                <div class="btn btn-primary btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/corporate.svg" width="92" height="233" alt="Balloons for corporate events icon" class="smallbouncer" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5 mob-py-0">
  <div class="row py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/wedding.svg" width="128"  height="268" alt="Balloons for weddings icon" class="smallbouncer" />
            </div>
          </div>
        </div>
        <div class="col-lg-5 pr-5 position-relative z-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-red">Weddings</h2>
              <p class="text-large">Brighten up your ceremony or reception room with our stunning balloon art.</p>
              <a href="{{route('services.weddings')}}">
                <div class="btn btn-red btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-5">
          <div class="behind-red d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/weddings.webp?v1.0" type="image/webp"/> 
                <source srcset="/img/temp/weddings.jpg?v1.0" type="image/jpeg"/> 
                <img srcset="/img/temp/weddings.jpg?v1.0" type="image/jpeg" alt="Balloons for weddings in the UK and Ireland" width="314" height="392" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-7 pr-5 text-center mob-px-4 position-relative z-2 d-lg-none">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-red mt-4">Weddings</h2>
              <p class="text-large">Brighten up your ceremony or reception room with our stunning balloon art.</p>
              <a href="{{route('services.weddings')}}">
                <div class="btn btn-red btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5 mob-py-0">
  <div class="row py-5">
    <div class="container mb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5">
          <div class="behind-yellow d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/parties.webp" type="image/webp"/> 
                <source srcset="/img/temp/parties.jpg" type="image/jpeg"/> 
                <img srcset="/img/temp/parties.jpg" type="image/jpeg" alt="Balloons for parties in the UK and Ireland" width="314" height="392" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-6 pr-5 text-center text-lg-left mob-px-5 position-relative z-2">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-yellow mt-4">Parties</h2>
              <p class="text-large">Perfect for birthdays, Chirstmas, christenings or other party events!</p>
              <a href="{{route('services.parties')}}">
                <div class="btn btn-yellow btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/party.svg" width="105" height="215" alt="Balloons for parties icon" class="smallbouncer" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<popular-products :category="'*'" class="pb-5"></popular-products>
<div class="container-fluid balloons-bg bg bg-fixed position-relative mt-5">
  <div class="trans"></div>
  <div class="row py-5 mob-py-0">
    <div class="container py-5">
      <div class="row py-5">
        <div class="col-12 text-center">
          <h2 class="text-primary">Customise Balloons</h2>
          <p class="text-primary"><b>Looking for something unique, with customised writing, colours and shapes?</b></p>
          <a href="{{route('custom-balloons')}}">
            <div class="btn btn-yellow btn-icon">Start Customising <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5 mob-px-4 position-relative z-2">
  <div class="row pt-5 mob-py-3">
    <div class="col-12 text-center mb-4">
      <h2 class="mb-4 text-primary">Balloon Blog</h2>
    </div>
    @foreach($posts as $key => $post)
    @if($key != 2)
    <div class="col-lg-4 col-md-6 mb-5">
    @else
    <div class="col-lg-4 col-md-6 d-block d-md-none d-lg-block mb-5">
    @endif
      <a href="{{route('blog-single', ['slug' => $post->slug, 'date' => $post->getDate($post->created_at)])}}">
        <div class="card border-0 shadow overflow-hidden post-box text-center text-md-left text-dark zoom-link">
          <div class="post-image zoom-img">
            <picture> 
              <source  srcset="{{$post->getFirstMediaUrl('blog', 'featured-webp')}}" type="image/webp"/> 
              <source srcset="{{$post->getFirstMediaUrl('blog', 'featured')}}" type="{{$post->getFirstMedia('blog')->mimetype}}"/> 
              <img src="{{$post->getFirstMediaUrl('blog', 'featured')}}" type="{{$post->getFirstMedia('blog')->mimetype}}" alt="{{$post->title}}" class="w-100" />
            </picture>
          </div>
          <div class="p-4">
            <p class="post-exerpt text-small mb-1 text-red">{{$post->getFancyDate($post->created_at)}}</p>
            <h4 class="post-title text-primary mb-2">{{$post->title}}</h4>
            <p class="post-exerpt text-small mb-3">{{substr($post->excerpt,0,100)}}...</p>
            <p class="mb-0 text-red"><b>Read more</b> <i class="fa fa-arrow-circle-right text-red ml-1"></i></p>
          </div>
        </div>
      </a>
    </div>
    @endforeach
    <div class="col-12 text-center">
      <a href="{{route('blog')}}">
        <div class="btn btn-red btn-icon">All blog posts <i class="fa fa-chevron-right"></i></div>
      </a>
    </div>
  </div>
</div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection
