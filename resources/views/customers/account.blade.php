@php
$page = 'Account Managment';
$pagetitle = 'Account Managment | Customer Dashboard - Luce Balloons';
$metadescription = '';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://www.luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.customer', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<div class="container pt-5 position-relative z-2">
  <div class="row pt-5 mt-5">
    <div class="col-12 mt-5">
        <h2 class="text-primary"><img src="/img/shapes/balloon-blue.svg" width="50" alt="luce ballons account" class="mr-3"/>Account Managment</h2>
    </div>
  </div>
</div>
@endsection
@section('content')
@if(session('action_status'))
<div id="notification_stripe" class="container position-relative z-2 py-3">
    <div class="row mob_text_center">
        <div class="col-12">
            <p class="mb-0 {{session('action_colour')}}"><b>{{session('action_status')}}: </b>{{session('action_message')}}</p>
        </div>
    </div>
</div>
@endif
@php 
session()->forget('action_status');
session()->forget('action_message');
session()->forget('action_colour');
@endphp
<div class="container position-relative pb-5 z-2">
    
    <div class="row py-5">
        <div class="col-md-12">
            <h3 class="">Your Details*</h3>
            <hr class="primary_line">
        </div>
        <div class="col-md-8">
            <p class=" mb-3">Only the information you provided at the point of purchase is stored. Your details are kept in a secure database so you may log in and use this system again in the future. If you wish to have your details removed from our system please <a href="{{route('contact')}}">contact us</a>.</p>
            <p class="text-red mb-4">* You can update your address details each time you make a new purchase</p>
        </div>
        <div class="col-sm-6">
            <p class="marg_top_20"><b>Name: </b>{{$currentUser->first_name}} {{$currentUser->last_name}}</p>
            <p class="marg_top_20"><b>Email: </b>{{$currentUser->email}}</p>
        </div>
        <div class="col-sm-6">
            <p class="marg_top_20"><b>Registered since: </b>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($currentUser->created_at))->format('d/m/Y') }}</p>
            <p class="marg_top_20"><b>Details last updated: </b>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($currentUser->updated_at))->format('d/m/Y') }}</p>
        </div>
    </div>
    
    <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Change Details</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">Change your name or email address using the form below. Note that after saving the changes, these will be the details we use to contact you.</p>
                </div>
            </div>
            <form action="{{route('update-account')}}" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-6 half_col mb-3">
                        <input name="first_name" type="text" class="form-control" placeholder="First Name" value="{{$currentUser->first_name}}"/>
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-3">
                        <input name="last_name" type="text" class="form-control" placeholder="Last Name" value="{{$currentUser->last_name}}"/>
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-12 half_col mb-3">
                        <input name="email" type="text" class="form-control" placeholder="New Email" value="{{$currentUser->email}}"/>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-12 half_col mb-3">
						<button type="submit" class="btn btn-primary float-right mt-0 px-5">Confirm</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Change Password</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">Change your password using the form below. Note that after saving your new password, you will need to use it to log in afterwards.</p>
                </div>
            </div>
            <form action="/change-password" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-4 half_col mb-3">
                        <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Current Password" required>
                        @if ($errors->has('current_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4 half_col mb-3">
                        <input id="password" type="password" class="form-control" name="password" placeholder="New Password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4 half_col mb-3">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm New Password" required>
                    </div>
                    <div class="col-md-12 half_col">
                        <button type="submit" class="btn btn-primary float-right mt-0">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{-- <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Remove Account</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">You can remove your account and all of your information from this web app. This action is irreversible and you will lose access to all the content that you have paid for. You can pay for a new account afterwards at any time.</p>
                </div>
            </div>
            <form action="/remove-account" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-4 half_col mb-3">
                        <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Current Password" required>
                        @if ($errors->has('current_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-8 half_col">
                        <button type="submit" class="btn btn-primary float-right mt-0">Remove Account</button>
                    </div>
                </div>
            </form>
        </div>
    </div> --}}
</div>
@endsection
@section('scripts')
<script>
    $('.possible-other').each(function(){
        var select = $(this).find('select');
        var input = $(this).find('input');
        $(select).on('change',function(){
            if($(select).val() == 'Other'){
                $(input).removeClass('d-none');
                $(input).focus();
            }
            else{
                $(input).val('');
                $(input).addClass('d-none');
            }
        })
    });
</script>
@endsection