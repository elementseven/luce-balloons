@php
$page = 'Terms & Conditions';
$pagetitle = 'Terms & Conditions - Luce Balloons | Balloons for parties, weddings, corporate events & special occasions';
$metadescription = 'Terms & Conditions';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative  py-5 mt-5 mob-mt-0 page-top overflow-hidden">
  <div class="row mt-5 justify-content-center position-relative z-2">
    <div class="col-12 mob-px-3 text-left mt-5">
    	<h1 class="text-primary">Terms & Conditions</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container position-relative z-2 pb-5 mb-5">
	<div class="row">
		<div class="col-12">
			<h3>1 ACCEPTANCE OF TERMS</h3>
			<p>Your access to and use of www.luceballoonsandcrafts.com (“the Website”) is subject exclusively to these Terms and Conditions. You will not use the Website for any purpose that is unlawful or prohibited by these Terms and Conditions. By using the Website you are fully accepting the terms, conditions and disclaimers contained in this notice. If you do not accept these Terms and Conditions you must immediately stop using the Website.</p>

			<h3 class="mt-5">2 ADVICE</h3>
			<p>The contents of the Website do not constitute advice and should not be relied upon in making or refraining from making, any decision.</p>

			<h3 class="mt-5">3 CHANGES TO WEBSITE</h3>
			<p>www.luceballoonsandcrafts.com reserves the right to:</p>
			<p>3.1 change or remove (temporarily or permanently) the Website or any part of it without notice and you confirm that www.luceballoonsandcrafts.com shall not be liable to you for any such change or removal; and</p>
			<p>3.2 change these Terms and Conditions at any time, and your continued use of the Website following any changes shall be deemed to be your acceptance of such change.</p>

			<h3 class="mt-5">4 LINKS TO THIRD PARTY WEBSITES</h3>
			<p>The Website may include links to third party websites that are controlled and maintained by others. Any link to other websites is not an endorsement of such websites and you acknowledge and agree that we are not responsible for the content or availability of any such sites.</p>

			<h3 class="mt-5">5 COPYRIGHT</h3>
			<p>5.1 All copyright, trade marks and all other intellectual property rights in the Website and its content (including without limitation the Website design, text, graphics and all software and source codes connected with the Website) are owned by or licensed to www.luceballoonsandcrafts.com or otherwise used by www.luceballoonsandcrafts.com as permitted by law.</p>
			<p>5.2 In accessing the Website you agree that you will access the content solely for your personal, non-commercial use. None of the content may be downloaded, copied, reproduced, transmitted, stored, sold or distributed without the prior written consent of the copyright holder. This excludes the downloading, copying and/or printing of pages of the Website for personal, non-commercial home use only.</p>

			<h3 class="mt-5">6 DISCLAIMERS AND LIMITATION OF LIABILITY</h3>

			<p>6.1 The Website is provided on an “AS IS” and “AS AVAILABLE” basis without any representation or endorsement made and without warranty of any kind whether express or implied, including but not limited to the implied warranties of satisfactory quality, fitness for a particular purpose, non-infringement, compatibility, security and accuracy.</p>
			<p>6.2 To the extent permitted by law, www.luceballoonsandcrafts.com will not be liable for any indirect or consequential loss or damage whatever (including without limitation loss of business, opportunity, data, profits) arising out of or in connection with the use of the Website.</p>
			<p>6.3 www.luceballoonsandcrafts.com makes no warranty that the functionality of the Website will be uninterrupted or error free, that defects will be corrected or that the Website or the server that makes it available are free of viruses or anything else which may be harmful or destructive.</p>
			<p>6.4 Nothing in these Terms and Conditions shall be construed so as to exclude or limit the liability of www.luceballoonsandcrafts.com for death or personal injury as a result of the negligence of www.luceballoonsandcrafts.com or that of its employees or agents.</p>

			<h3 class="mt-5">7 INDEMNITY</h3>
			<p>You agree to indemnify and hold www.luceballoonsandcrafts.com and its employees and agents harmless from and against all liabilities, legal fees, damages, losses, costs and other expenses in relation to any claims or actions brought against www.luceballoonsandcrafts.com arising out of any breach by you of these Terms and Conditions or other liabilities arising out of your use of this Website.</p>

			<h3 class="mt-5">8 SEVERANCE</h3>
			<p>If any of these Terms and Conditions should be determined to be invalid, illegal or unenforceable for any reason by any court of competent jurisdiction then such Term or Condition shall be severed and the remaining Terms and Conditions shall survive and remain in full force and effect and continue to be binding and enforceable.</p>

			<h3 class="mt-5">9 GOVERNING LAW</h3>
			<p>These Terms and Conditions shall be governed by and construed in accordance with the law of Northern Ireland and you hereby submit to the exclusive jurisdiction of the Northern Ireland courts.</p>
			<p>Should you have any concerns about privacy please click here.</p>
			<p>Should you have any queries regarding our Terms of Use then please do not hesitate to contact us.</p>
		</div>
	</div>
</div>
@endsection


