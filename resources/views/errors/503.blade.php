<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <title>Luce Balloons - Down For Maintenance</title>

  <meta property="og:type" content="website">
  <meta property="og:title" content="Luce Balloons - Down For Maintenance">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Luce Balloons">
  <meta property="og:locale" content="en_GB">
  <meta property="og:description" content="Luce Balloons">
  <meta name="description" content="Luce Balloons">
  <link href="{{ mix('css/app.css') }}" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "+442892673718",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>

</head>
<body class="front">
  @yield('fbroot')
  <div id="main-wrapper">
    <div id="app" class="front">
      <div id="menu_btn" class="menu_btn float-left d-lg-none"><div class="nav-icon"><span></span><span></span><span></span></div></div>
      <div id="main-menu" class="menu">
        <div id="main-menu-top" class="container-fluid px-5 ipad-px-3 mob-px-1 bg-primary py-1 mob-py-1 overflow-hidden text-white d-none d-lg-block">
          <div class="row">
            <div class="col-lg-7">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="mailto:eric@luceballoons.co.uk" class="text-white"><i class="fa fa-envelope mr-1"></i> eric@luceballoons.co.uk</a>
                <a href="tel:004402892673718" class="text-white ml-4"><i class="fa fa-phone mr-1"></i> +44 28 9267 3718</a>
              </p>
            </div>
            <div class="col-lg-5 text-center text-lg-right mob-px-0 d-none d-lg-block">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="https://www.facebook.com/Luce.Balloons/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-facebook ml-4"></i></a>
                <a href="https://www.instagram.com/luceballoons/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-instagram ml-3"></i></a>
                <a href="https://twitter.com/Luceballoons" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
                <a href="https://www.linkedin.com/company/luce-balloons" class="text-white d-none d-lg-inline"><i class="fa fa-linkedin ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
        <div class="container-fluid px-5 ipad-px-3 mob-px-3">
          <div class="row">
            <div class="col-lg-2 mob-mt-2 py-2">
              <a href="/">
                <img src="/img/logos/logo.svg" class="menu_logo" alt="Luce Balloons Logo" width="98" />
              </a>
            </div>
            
          </div>
        </div>
      </div>
      <div id="scroll-menu" class="menu d-none d-lg-block">
        <div id="main-menu-top" class="container-fluid px-5 ipad-px-3 mob-px-1 bg-primary py-1 mob-py-1 overflow-hidden text-white d-none d-lg-block">
          <div class="row">
            <div class="col-lg-9">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="mailto:eric@luceballoons.co.uk" class="text-white"><i class="fa fa-envelope mr-1"></i> eric@luceballoons.co.uk</a>
                <a href="tel:00442892673718" class="text-white ml-4"><i class="fa fa-phone mr-1"></i> +44 28 9267 3718</a>
              </p>
            </div>
            <div class="col-lg-3 text-center text-lg-right mob-px-0 d-none d-lg-block">
              <p class="d-block d-lg-inline-block my-1 text-small">
                <a href="https://www.facebook.com/Luce.Balloons/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-facebook ml-4"></i></a>
                <a href="https://www.instagram.com/luceballoons/" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-instagram ml-3"></i></a>
                <a href="https://twitter.com/Luceballoons" class="text-white d-none d-lg-inline" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
                <a href="https://www.linkedin.com/company/luce-balloons" class="text-white d-none d-lg-inline"><i class="fa fa-linkedin ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
        <div class="container-fluid px-5 ipad-px-3 ">
          <div class="row">
            <div class="col-4 col-lg-2 pb-3 pt-2">
              <a href="/">
                <img src="/img/logos/logo.svg" class="menu_logo" alt="Luce Balloons Logo"/>
              </a>
            </div>
            <div class="col-8 col-lg-10 text-right pt-4 mt-3 d-none d-lg-block">
             
  
            </div>
          </div>
        </div>
      </div>

      <div id="mobile-menu" class="mobile-menu">
        <div class="container-fluid px-3">
         <div class="row">
          <a href="/">
            <img src="/img/logos/logo.svg" class="menu_logo" width="60" alt="Luce Balloons Logo"/>
          </a>
          <div class="col-lg-10 pt-5 mt-5">
            <div class="menu-links d-block ">
              <p class="mb-0 text-large mt-3">
                <a href="https://www.facebook.com/Luce.Balloons"><i class="fa fa-facebook"></i></a>
                <a href="https://www.instagram.com/luceballoons"><i class="fa fa-instagram ml-3"></i></a>
                <a href="https://twitter.com/Luceballoons" ><i class="fa fa-twitter ml-3"></i></a>
                <a href="https://www.linkedin.com/company/luce-balloons" ><i class="fa fa-linkedin ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @yield('header')
    <main id="content" style="z-index: 2;">
      <div id="menu-trigger"></div>
      <div class="container px-5 text-center  py-5 my-5 position-relative z-2" style="min-height: 600px;">
	      <h1 class="text-primary mt-5 pt-5 mob-pt-0">We'll be<br class="d-lg-none"/> back soon!</h1>
				<p>Our website is undergoing maintenance, please check back later.</p>
				<a href="https://www.facebook.com/Luce.Balloons/" target="_blank">
					<button class="btn btn-red">Visit our Facebook</button>
				</a>
			</div>
    </main>
    <footer class="container-fluid text-center text-lg-left position-relative z-1 bg-primary">
      <div class="position-relative footer-inner row py-5 mob-mt-0 bg-primary">
        <div class="container pt-5">
          <div class="row">
            <div class="col-lg-3 mb-3 pr-5 mob-px-3 ipadp-px-3 text-center">
              <img src="/img/logos/logo-white.svg" alt="Luce Balloons logo" class="mb-4" />
              <p class="mb-2">
                <a href="https://www.facebook.com/Luce.Balloons"><i class="fa fa-facebook-square"></i></a>
                <a href="https://www.instagram.com/luceballoons"><i class="fa fa-instagram ml-2"></i></a>
                <a href="https://twitter.com/Luceballoons"><i class="fa fa-twitter ml-2"></i></a>
                <a href="https://www.linkedin.com/company/luce-balloons" ><i class="fa fa-linkedin ml-2"></i></a>
              </p>
            </div>
            <div class="col-lg-3 offset-lg-6 mb-3 text-lg-right">
              <p>Unit 12,<br>Rosevale Industrial Estate,<br> 171 Moira Rd, <br>Lisburn BT28 1RW</p>
              <p class="mb-1"><a href="tel:00442892673718">+44 28 9267 3718</a></p>
              <p><a href="mailto:eric@luceballoons.co.uk">eric@luceballoons.co.uk</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row text-center bg-primary">
        <div class="col-12">
          <img src="/img/logos/payment.svg" width="169" class="mb-3" alt="We accept mastercard, visa and other major card payment options"/>
          <p class="text-smaller">&copy;{{Carbon\Carbon::now()->format('Y')}} Luce Balloons Ltd | <a href="https://elementseven.co" target="_blank">Website by Element Seven</a></p>
        </div>
      </div>
    </footer>
    @yield('modals')
  </div>
  <div id="menu_body_hide"></div>
  <div id="loader">
    <div class="vert-mid">
      <div class="card p-5">
        <img id="loader-success" src="/img/icons/success.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
        <img id="loader-error" src="/img/icons/error.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
        <div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <div id="loader-message"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                  <p id="loader-second-text" class="mt-3 d-none cursor-pointer"><a id="loader-second-link"></a></p>
                </div>
                <div class="col-md-6">
                  @yield('loader-buttons')
                  <a id="loader-link">
                    <div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
                  </a>
                  <div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@yield('prescripts')
<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
<script>
  window.addEventListener("load", function(){
   window.cookieconsent.initialise({
     "palette": {
       "popup": {
         "background": "#1D3D8D",
         "text": "#ffffff"
       },
       "button": {
         "background": "#E73C62",
         "text": "#ffffff",
         "border-radius": "30px"
       }
     }
   })});
 </script>
 @yield('scripts')
</body>
</html>