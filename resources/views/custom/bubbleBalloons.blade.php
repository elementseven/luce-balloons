@php
$page = 'Bubble Balloons';
$pagetitle = 'Bubble Balloons | Bubble Balloons for parties, weddings, corporate events & special occasions';
$metadescription = 'Bubble Balloons Luce Balloons is a professional balloon company that started trading in 1997. We started out as a home based business and have steadily grown into one of the premier balloon companies in Northern Ireland.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid page-top position-relative py-5 overflow-x-hidden">
	<img src="/img/shapes/circle-light-blue.svg" class="circle-red" alt="Luce balooons red circle"/>
	<div class="row py-5 mob-py-0">
		<div class="container py-5">
		  <div class="row pt-5">
		    <div class="col-lg-10 text-center text-lg-left">
		      <h1 class="mb-4 page-top text-primary mb-4"><span class="smaller">Northern Ireland's favourite </span><br><span class="larger text-red">Bubble Balloons</span></h1>
		      <p class="text-larger mb-4">Bubble Balloons are growing ever more popular in Northern Ireland for birthdays, corporate parties and many other events. Find out more about bubble balloons below or create your own custom bubble balloon with our bubble balloon builder!</p>
		      <div id="scroll-to" class="btn btn-red btn-icon">Build a balloon <i class="fa fa-chevron-down"></i></div>
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-2 mb-5 pb-5">
  <div class="row">
    <div class="container mob-pb-0 mob-px-4">
      <div class="row">
        <div class="col-lg-5 mb-4">
        	<div id="bubbleCarousel" class="carousel slide" data-ride="carousel">
					  <div class="carousel-inner shadow rounded-image text-center">
					    <div class="carousel-item active">
					      <picture> 
								  <source  srcset="/img/customballoons/bubble-1.webp" type="image/webp"/> 
								  <source srcset="/img/customballoons/bubble-1.jpg" type="image/jpeg"/> 
								  <img src="/img/customballoons/bubble-1.jpg" type="image/jpeg" alt="bubble balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
								</picture>
					    </div>
					    <div class="carousel-item">
					      <picture> 
								  <source  srcset="/img/customballoons/bubble-2.webp" type="image/webp"/> 
								  <source srcset="/img/customballoons/bubble-2.jpg" type="image/jpeg"/> 
								  <img src="/img/customballoons/bubble-2.jpg" type="image/jpeg" alt="bubble balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
								</picture>
					    </div>
					    <div class="carousel-item">
					      <picture> 
								  <source  srcset="/img/customballoons/bubble-3.webp" type="image/webp"/> 
								  <source srcset="/img/customballoons/bubble-3.jpg" type="image/jpeg"/> 
								  <img src="/img/customballoons/bubble-3.jpg" type="image/jpeg" alt="bubble balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
								</picture>
					    </div>
					    <div class="carousel-item">
					      <picture> 
								  <source  srcset="/img/customballoons/bubble-4.webp" type="image/webp"/> 
								  <source srcset="/img/customballoons/bubble-4.jpg" type="image/jpeg"/> 
								  <img src="/img/customballoons/bubble-4.jpg" type="image/jpeg" alt="bubble balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
								</picture>
					    </div>
					    <div class="carousel-item">
					      <picture> 
								  <source  srcset="/img/customballoons/bubble-5.webp" type="image/webp"/> 
								  <source srcset="/img/customballoons/bubble-5.jpg" type="image/jpeg"/> 
								  <img src="/img/customballoons/bubble-5.jpg" type="image/jpeg" alt="bubble balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
								</picture>
					    </div>
					    <div class="carousel-item">
					      <picture> 
								  <source  srcset="/img/customballoons/bubble-6.webp" type="image/webp"/> 
								  <source srcset="/img/customballoons/bubble-6.jpg" type="image/jpeg"/> 
								  <img src="/img/customballoons/bubble-6.jpg" type="image/jpeg" alt="bubble balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
								</picture>
					    </div>
					  </div>
					  <a class="carousel-control-prev" href="#bubbleCarousel" role="button" data-slide="prev">
					    <span class="" aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#bubbleCarousel" role="button" data-slide="next">
					    <span aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
					    <span class="sr-only">Next</span>
					  </a>
					</div>
          
        </div>
        <div class="col-lg-7 pl-5 mob-px-3">
        	<h2 class="text-primary mb-3">Balloons inside balloons!</h2>
        	<p>With our magical blend of “Fairy Dust and Wizardry” we can create a personalised balloon for that special someone.</p>
					<p>We offer a wide range of customisation options for our bubble balloons. With lots of colour combinations to choose from, you can be sure that your bubble will perfectly match the theme and style of your event, as well as being an excellent photo opportunity! Whether you’re looking for bold, vibrant colours or soft, muted tones, we’ve got you covered. </p>

					<p>You can also add your own personal message or logo to your bubble, with a wide range of fonts and colours to choose from. Whether you want to wish someone a Happy Birthday, congratulate them on a job well done, or simply add a fun message. Our team of designers will work with you to create the perfect one-of-a-kind design.</p>

					<p>And because our bubble balloons are made from a special kind of plastic, they are durable and long lasting. This means that your personalised message will stay looking great for the duration of your event and longer making it a truly unique gift that everyone will love.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="scroll-to-element" class="container-fluid position-relative z-2 mb-5">
  <div class="row">
    <div class="container mob-pb-0 mob-px-4">
      <div class="row justify-content-center">
        <div class="col-lg-10 text-center mb-4">
        	<h2 class="ml-3 text-primary">Build your own bubble balloon</h2>
        	<p class="text-large">Create your own custom bubble balloon with our bubble balloon builder! These balloons can be further customised after purchase, just let us know what you need in the form below.</p>
        </div>
        <div class="col-12">
        	<bubble-balloons></bubble-balloons>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection
@section('scripts')
<script>
  $("#scroll-to").click(function() {
    $([document.documentElement, document.body]).animate({
      scrollTop: $("#scroll-to-element").offset().top -200
    }, 300);
  });
</script>
@endsection
@section('modals')

@endsection