@php
$page = 'Custom Balloons';
$pagetitle = 'Custom Balloons - Luce Balloons';
$metadescription = 'Browse our Custom Balloons';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
@import url('https://fonts.googleapis.com/css2?family=Lora&family=Montserrat&display=swap');
</style>
@endsection
@section('header')
<header class="container position-relative pt-5 z-2">
  <div class="row pt-5 mob-pt-0">
    <div class="col-lg-12 mt-5 text-center text-lg-left">
      <div class="row">
        @if(isset($_GET["type"]))
      	<custom-balloons :type="{{$_GET["type"]}}"></custom-balloons>
        @else
        <custom-balloons></custom-balloons>
        @endif
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection