@php
$page = 'Customer Dashboard';
$pagetitle = 'Customer Dashboard - Luce Balloons';
$metadescription = '';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://www.luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.customer', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<div class="container pt-5 position-relative z-2">
  <div class="row pt-5 mt-5">
    <div class="col-12 mt-5">
      <h2 class="text-primary"><img src="/img/shapes/balloon-blue.svg" width="50" alt="luce ballons account" class="mr-3"/>Welcome {{$currentUser->first_name}}!</h2>
    </div>
  </div>
</div>
@endsection
@section('content')
<div class="container position-relative z-2">
  <div class="row py-5">
    <div class="col-12 mt-5">
      <h3 class="mb-3">Your Purchases</h3>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Invoice Id</th>
              <th scope="col">Transaction Reference</th>
              <th scope="col">Date</th>
              
              <th scope="col">Total</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($invoices as $i)
            <tr>
              <th scope="row">{{$i->id}}</th>
              <td>{{$i->transaction_id}}</td>
              <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $i->created_at)->format('d/m/Y')}}</td>
              <td>£{{number_format($i->total + $i->shipping, 2, '.', ' ')}}</td>
              <td class="text-lg-right">
                <p class="mb-0">
                  <a class="text-primary" data-toggle="collapse" href="{{'#invoice_details_' . $i->id}}" role="button" aria-expanded="false" aria-controls="{{'invoice_details_' . $i->id}}">
                    <b>View Details</b>
                  </a>
                </p>
              </td>
            </tr>
            <tr>
              <td colspan="5" class="p-0 border-0">
                <div class="collapse" id="{{'invoice_details_' . $i->id}}">
                  <div class="card card-body mb-4">
                    <div class="row">
                      <div class="col-lg-6">
                        <p class="mb-2 text-small"><b>Order Value:</b> £{{number_format($i->total, 2, '.', ' ')}}</p>
                        <p class="mb-2 text-small"><b>Shipping:</b> £{{number_format($i->shipping, 2, '.', ' ')}}</p>
                        <p class="mb-2 text-small"><b>Shipping Type:</b> {{$i->shipping_type}} @if($i->shipping_type == "Click and collect") on {{Carbon\Carbon::parse($i->collection_date)->format('d/m/Y')}}@endif</p>
                        @if($i->event_date)<p class="mb-2 text-small"><b>Event Date:</b> {{Carbon\Carbon::parse($i->event_date)->format('d/m/Y')}}</p>@endif
                      </div>
                      <div class="col-lg-6">
                        <p class="mb-1 text-small"><b>Items:</b></p>
                        @foreach($i->variants as $v)
                        {{$v->product->title}} - x{{$v->pivot->qty}} <br/>
                        @endforeach
                        @foreach($i->customorders as $co)

                        @if($co->decorthrees)
                        Custom decor three
                        @endif

                        @if($co->decorfives)
                        Custom decor five
                        @endif

                        @if($co->bubbleballoons)
                        Custom bubble balloon
                        @endif

                        @if($co->foilballoons)
                        Custom foil balloon
                        @endif

                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="container position-relative z-2 py-5 mb-5">
  <div class="row">
    <div class="col-12">
      <h3 class="mb-3">Account details</h3>
    </div>
    <div class="col-lg-4">
      <p class="mb-0"><b>Your details:</b></p>
      <p class="mb-0">{{$currentUser->full_name}}</p>
      @if($currentUser->phone){{$currentUser->phone}}</p>@endif
      <p class="mb-0">{{$currentUser->email}}</p>
      <p class="mb-0">Account created: {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $currentUser->created_at)->format('d/m/Y')}}</p>
    </div>
    <div class="col-lg-4">
      <p class="mb-0"><b>Shipping Address:</b></p>
      <p class="mb-0 text-uppercase">@if($shippingaddress->company){{$shippingaddress->company}}, <br/>@endif{{$shippingaddress->streetAddress}}, @if($shippingaddress->extendedAddress){{$shippingaddress->streetAddress}}, @endif <br/>{{$shippingaddress->city}}, {{$shippingaddress->postalCode}},<br/> {{$shippingaddress->region}}, {{$shippingaddress->country}}</p>
    </div>
    <div class="col-lg-4">
      <p class="mb-0"><b>Billing Address:</b></p>
      <p class="mb-0 text-uppercase">@if($billingaddress->company){{$billingaddress->company}}, <br/>@endif{{$billingaddress->streetAddress}}, @if($billingaddress->extendedAddress){{$billingaddress->streetAddress}}, @endif <br/>{{$billingaddress->city}}, {{$billingaddress->postalCode}},<br/> {{$billingaddress->region}}, {{$billingaddress->country}}</p>
    </div>
    <div class="col-12">
      <hr/>
      <p><b><a href="{{route('account-management')}}"><i class="fa fa-edit mr-2"></i>Update your details</a></b></p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection