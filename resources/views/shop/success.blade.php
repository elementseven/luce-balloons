@php
$page = 'Success';
$pagetitle = 'Success | Luce Balloons';
$metadescription = 'Success - Luce Balloons';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 mt-5 mob-mt-0">
  <div class="row pt-5">
    <div class="col-lg-12">
      <div class="pt-5">
        <p class="text-primary text-small text-uppercase letter-spacing mb-0" data-aos="fade-in"><b>Thank you!</b></p>
        <h1 class="pb-5 mb-0 text-primary" data-aos="fade-in" data-aos-delay="100">Payment Successful</h1>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container position-relative z-2">
  <div class="row">
    <div class="col-12">
      <div class="pb-5 mb-5">
        <h3 class="mb-4 text-primary">Thank you for your payment</h3>
        <p class="text-large mb-5">You will receive an email to <span class="text-primary">{{$invoice->user->email}}</span> confirming your login and order details shortly. <br/><u>Please remember to check your inbox and your spam folder</u>.</p>
         
        @if(count($invoice->variants))<h3 class="mt-4">Products:</h3>@endif

        @foreach($invoice->variants as $key => $variant)
        <div class="row">
          <div class="col-12">
            <p class="text-large">{{$variant->product->title}} - {{$variant->title}} - @if($variant->sale_price) £{{$variant->sale_price}} @else £{{$variant->price}} @endif - x{{$variant->pivot->qty}}</p>
            @if($key != count($invoice->variants) - 1)<hr/>@endif
          </div>
        </div>
        @endforeach
        @if(count($invoice->customorders))
        <h4>Custom Items:</h4>
        @foreach($invoice->customorders as $key => $co)

        @if($co->decorthrees)
        <div class="row">
          <div class="col-12">
            <p class="text-large">Custom bunch of 3 balloons - £{{$co->price}} - x{{$co->qty}}</p>
            @if($key != count($invoice->customorders) - 1)<hr/>@endif
          </div>
        </div>
        @endif

        @if($co->decorfives)
        <div class="row">
          <div class="col-12">
            <p class="text-large">Custom bunch of 5 balloons - £{{$co->price}} - x{{$co->qty}}</p>
            @if($key != count($invoice->customorders) - 1)<hr/>@endif
          </div>
        </div>
        @endif

        @if($co->bubbleballoons)
        <div class="row">
          <div class="col-12">
            <p class="text-large">Custom bubble balloon - £{{$co->price}} - x{{$co->qty}}</p>
            @if($key != count($invoice->customorders) - 1)<hr/>@endif
          </div>
        </div>
        @endif

        @if($co->foilballoons)
        <div class="row">
          <div class="col-12">
            <p class="text-large">Custom foil balloon - £{{$co->price}} - x{{$co->qty}}</p>
            @if($key != count($invoice->customorders) - 1)<hr/>@endif
          </div>
        </div>
        @endif
        @endforeach
        @endif
        @if($invoice->notes)
        <div class="row mt-4">
          <div class="col-12">
            <h4>Order Notes:</h4>
            <p>{{$invoice->notes}}</p>
          </div>
        </div>
        @endif

        <div class="row mt-4"> 
          @foreach($invoice->user->addresses as $address)
          <div class="col-lg-4 col-md-6">
            <h4 class="text-capitalize">{{$address->type}} address</h4>
            <p class="text-large">{{$address->streetAddress}},<br/>@if($address->extendedAddress){{$address->extendedAddress}},<br/>@endif @if($address->region){{$address->region}},<br/>@endif{{$address->city}},{{$address->postalCode}},<br/>{{$address->country}}</p>
          </div>
          @endforeach
        </div>
        <p class="mt-5 mb-1"><b>Order Value:</b> &pound;{{number_format($invoice->total, 2)}} </p>
        <p class="mb-1"><b>Shipping:</b> &pound;{{number_format($invoice->shipping, 2)}} </p>
        <h3 class="mt-4">Total:</b> &pound;{{number_format($invoice->total + $invoice->shipping, 2)}} </h3>
        <hr class="line primary-line my-5"/>
        <p class="text-large mb-3">We will be in touch shortly to make any necessary arrangements.</p>
        <a href="/dashboard">
          <div class="btn btn-primary">Customer Dashboard</div>
        </a>
      </div>   
    </div>
  </div>
</div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection