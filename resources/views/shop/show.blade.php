@php
$page = 'Blog';
$pagetitle = $product->title . ' | Luce Balloons';
$metadescription = $product->excerpt;
$pagetype = 'light';
$pagename = 'blog';
$ogimage = 'https://luceballoons.co.uk' . $product->getFirstMediaUrl('featured');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5 mob-mb-5">
    <div class="col-xl-12 pt-5 mob-px-4 mb-5 mob-mb-0">
      <p class="pt-5 mob-pt-0 mb-2"><b><a href="{{route('shop')}}"><i class="fa fa-arrow-circle-left"></i>&nbsp; Browse shop</a></b></p>
      <h1 class="text-primary blog-title mb-0">{{$product->title}}</h1>
      @if($product->hasType(1))
      <p class="text-red text-small">*This product is only available for shipping in Northern Ireland</p>
      @endif
    </div>
  </div>
</header>
@endsection
@section('content')
<show-product :product="{{$product}}"></show-product>
<div class="container mob-px-4 position-relative z-2">
    <div class="row pb-5 mob-py-0">
        <div class="col-12 text-center text-lg-left">
          <p class="mb-1 text-larger"><b>Share this product:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($product->title)}}&amp;summary={{urlencode($product->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($product->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($product->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
{{-- <div class="container py-5 mob-px-3 position-relative z-2">
    <div class="row">
        <div class="col-12 text-center mb-4">
            <h2 class="mb-4 text-red">Related Products</h2>
        </div>
        @foreach($others as $key => $product)
        <div class="col-md-4 mb-5">
          <a href="{{route('show-product', ['slug' => $product->slug])}}">
            <div class="shadow overflow-hidden product-box text-center text-md-left text-dark zoom-link">
              <div class="product-image zoom-img">
                <picture> 
                  <source  srcset="{{$product->getFirstMediaUrl('featured', 'normal-webp')}}" type="image/webp"/> 
                  <source srcset="{{$product->getFirstMediaUrl('featured', 'normal')}}" type="{{$product->getFirstMedia('featured')->mimetype}}"/> 
                  <img src="{{$product->getFirstMediaUrl('featured', 'normal')}}" type="{{$product->getFirstMedia('featured')->mimetype}}" alt="{{$product->title}}" class="w-100" />
                </picture>
              </div>
            </div>
            <p class="product-title text-dark mb-0 mt-2"><b>{{$product->title}}</b></p>
            <p class="product-exerpt text-small mb-0"><span class="smaller">From:</span> £{{$product->price}}</p>
          </a>
        </div>
        @endforeach
    </div>
</div> --}}
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection