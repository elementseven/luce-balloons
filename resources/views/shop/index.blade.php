@php
$page = 'Shop';
$pagetitle = 'Shop - Luce Balloons';
$metadescription = 'Browse our products';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5">
    <div class="col-lg-12 mt-5 mob-mt-0 text-center text-lg-left">
      <h1 class="mt-5 mb-4 text-primary">Shop</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<shop-index :categories="{{$categories}}" :types="{{$types}}"></shop-index>
<div class="container position-relative z-2">
    <div class="row">
      <div class="col-12">
        <h2 class="text-primary mb-4">Custom Balloons</h2>
      </div>
      <div class="col-lg-3 col-6 mb-4">
        <div class="card border-0 shadow p-0">
          <picture> 
            <source  srcset="/img/customballoons/bubble.webp" type="image/webp"/> 
            <source srcset="/img/customballoons/bubble.jpg" type="image/jpeg"/> 
            <img src="/img/customballoons/bubble.jpg" type="image/jpeg" alt="bubble balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
          </picture>
          <div class="p-4 text-center">
            <h4>Bubble Balloons</h4>
            <a href="/custom-balloons?type=1">
              <button type="button" class="btn btn-red" >Select</button>
            </a>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6 mb-4">
        <div class="card border-0 shadow p-0">
          <picture> 
            <source  srcset="/img/customballoons/foil.webp" type="image/webp"/> 
            <source srcset="/img/customballoons/foil.jpg" type="image/jpeg"/> 
            <img src="/img/customballoons/foil.jpg" type="image/jpeg" alt="foil balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
          </picture>
          <div class="p-4 text-center">
            <h4>Foil Balloons</h4>
            <a href="/custom-balloons?type=2">
              <button type="button" class="btn btn-red">Select</button>
            </a>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6 mb-4">
        <div class="card border-0 shadow p-0">
          <picture> 
            <source  srcset="/img/customballoons/3.webp" type="image/webp"/> 
            <source srcset="/img/customballoons/3.jpg" type="image/jpeg"/> 
            <img src="/img/customballoons/3.jpg" type="image/jpeg" alt="Elegant Decor 3 custom balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
          </picture>
          <div class="p-4 text-center">
            <h4>Bunch of 3 Balloons</h4>
            <a href="/custom-balloons?type=3">
              <button type="button" class="btn btn-red" >Select</button>
            </a>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6 mb-4">
        <div class="card border-0 shadow p-0">
          <picture> 
            <source  srcset="/img/customballoons/5.webp" type="image/webp"/> 
            <source srcset="/img/customballoons/5.jpg" type="image/jpeg"/> 
            <img src="/img/customballoons/5.jpg" type="image/jpeg" alt="Elegant Decor 5 custom balloons Luce Balloons - Balloons for events in the UK and Ireland" class="w-100" />
          </picture>
          <div class="p-4 text-center">
            <h4>Bunch of 5 Balloons</h4>
            <a href="/custom-balloons?type=5">
              <button type="button" class="btn btn-red" >Select</button>
            </a>
          </div>
        </div>
      </div>
      </div>
    </div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection