@php
$page = 'Checkout';
$pagetitle = 'Checkout | Luce Balloons';
$metadescription = 'Checkout - Luce Balloons';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])

@php 
	$hasAddress = false;
	$sameAddress = false;
	$billingAddress = null;
	$shippingAddress = null;
	if(Auth::check()){
		if(count($currentUser->addresses)){
			$hasAddress = true;
			foreach($currentUser->addresses as $a){
				if($a->type == 'billing'){
					$billingAddress = $a;
				}else if($a->type == 'shipping'){
					$shippingAddress = $a;
				}
			}
			if($billingAddress && $billingAddress->streetAddress == $shippingAddress->streetAddress){
				$sameAddress = true;
			}
		}
	}
	$hasMerch = false;
	
@endphp

@section('header')
<header class="container position-relative pt-5 mob-pt-0 z-2">
	<div class="row pt-5">
		<div class="col-lg-12 mt-5">
			<h1 class="page-title pt-5 text-primary">Checkout</h1>
			<p>If you would like your order for an earlier date, please get in touch or give us a ring on <a href="tel:00442892673718">02892673718</a></p>
		</div>
	</div>
</header>
@endsection

@section('content')
@if($shippingaddress && $billingaddress)
<checkout :amount="{{Cart::total(2,'.','')}}" :stripekey="'{{ env("STRIPE_KEY") }}'" login="{{Auth::check()}}" :billingaddress="{{$billingaddress}}" :shippingaddress="{{$shippingaddress}}" :inflated="{{$inflated}}"></checkout>
@else
<checkout :amount="{{Cart::total(2,'.','')}}" :stripekey="'{{ env("STRIPE_KEY") }}'" login="{{Auth::check()}}" :inflated="{{$inflated}}"></checkout>
@endif
@endsection

@section('prescripts')
<script src="https://js.stripe.com/v3/"></script>
@endsection
@section('scripts')
@endsection
