@php
$page = 'Basket';
$pagetitle = 'Basket | Luce Balloons';
$metadescription = 'Basket - Luce Balloons';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 mob-pt-0 z-2">
	<div class="row pt-5">
		<div class="col-lg-12 mt-5">
			@if(count(Cart::content()))
			<p class="mb-0 pt-5 text-small"><a href="{{route('shop')}}"><i class="fa fa-angle-double-left"></i> Continue Shopping</a></p>
			<h1 class="page-title text-primary mob-mb-4">Basket</h1>
			@else
			<h1 class="page-title text-primary text-center pt-5 mb-0">Basket Empty</h1>
			<p class="mb-4 text-center">You have nothing in your basket!</p>
			<div class=" text-center">
				<a href="{{route('shop')}}">
					<div class="btn btn-primary btn-icon mb-5">Browse Shop <i class="fa fa-chevron-right"></i></div>
				</a>
			</div>
			@endif
		</div>
	</div>
</header>
@endsection
@section('content')
@if(count(Cart::content()))
<basket :content="{{Cart::content()}}" :total="'{{Cart::total()}}'"></basket>
@else
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endif
@endsection
@section('scripts')
@endsection