@php
$page = 'Homepage';
$pagetitle = 'Contact Luce Balloons | Balloons for parties, weddings, corporate events & special occasions';
$metadescription = 'Balloons for corporate events, weddings, parties, special occasions, balloon releases and helium supplier based in Lisburn covering Belfast and Northern Ireland / NI';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 text-center text-lg-left">
  <div class="row pt-5 mt-5 mob-mt-0">
    <div class="col-lg-8 pt-5">
      <h1 class="mb-3 smaller-title text-primary">Get in touch</h1>
      <p class="text-large mb-4">If you have any questions at all, please get in touch using the form below.</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div id="article-body" class="container">
  <div class="row">
    <div class="col-lg-9 mb-5 pr-5 mob-px-3">
      <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
    </div>
    <div class="col-lg-3 text-center text-lg-left">
      <h4 class="text-red"><b>Contact us</b></h4>
      <p class="mb-1"><a href="tel:00442892673718"><i class="fa fa-phone mr-2"></i> 028 9267 3718</a></p>
      <p class="mb-4"><a href="mailto:eric@luceballoons.co.uk"><i class="fa fa-envelope mr-2"></i> eric@luceballoons.co.uk</a></p>
      <h4 class="text-red"><b>Find us</b></h4>
      <p class="">Unit 12, Rosevale Industrial Estate, 171 Moira Rd,<br>Lisburn BT28 1RW</p>
      <p><a href="https://what3words.com/still.awards.slim" target="_blank"><u>Find us on what3words</u></a></p>
    </div>
  </div>
</div>
<div id="map" class="position-relative z-2 my-5"></div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfBSo8hG8i7xKtvMvjHR8kMb40lFuJh60&region=GB&language=en-gb"></script>
<script>
    var marker;
    var map;
    var mapstyle = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"administrative","elementType":"labels","stylers":[{"saturation":"-100"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"gamma":"0.75"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"lightness":"-37"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f9f9f9"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"saturation":"-100"},{"lightness":"40"},{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"labels.text.fill","stylers":[{"saturation":"-100"},{"lightness":"-37"}]},{"featureType":"landscape.natural","elementType":"labels.text.stroke","stylers":[{"saturation":"-100"},{"lightness":"100"},{"weight":"2"}]},{"featureType":"landscape.natural","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":"-100"},{"lightness":"80"}]},{"featureType":"poi","elementType":"labels","stylers":[{"saturation":"-100"},{"lightness":"0"}]},{"featureType":"poi.attraction","elementType":"geometry","stylers":[{"lightness":"-4"},{"saturation":"-100"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"},{"visibility":"on"},{"saturation":"-95"},{"lightness":"62"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road","elementType":"labels","stylers":[{"saturation":"-100"},{"gamma":"1.00"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"gamma":"0.50"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"saturation":"-100"},{"gamma":"0.50"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"},{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"-13"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"lightness":"0"},{"gamma":"1.09"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"},{"saturation":"-100"},{"lightness":"47"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"lightness":"-12"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"},{"lightness":"77"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"lightness":"-5"},{"saturation":"-100"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"saturation":"-100"},{"lightness":"-15"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"lightness":"47"},{"saturation":"-100"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#B0D8F9"}]},{"featureType":"water","elementType":"geometry","stylers":[{"saturation":"53"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":"-42","color":"#ffffff"},{"saturation":"17"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"lightness":"61"}]}];
    function initMap(){
        var mapDiv = document.getElementById('map');
        var mapcenter = {lat:54.5061005, lng:-6.076193};
        var myIcon = new google.maps.MarkerImage("/img/icons/map-marker-alt.png", null, null, null, new google.maps.Size(30, 40));
        map = new google.maps.Map(mapDiv, {
          center: mapcenter,
          mapTypeControl: false,
          streetViewControl: false,
          fullscreenControl: false,
          zoom: 11
        });
        marker = new google.maps.Marker({
            position: mapcenter,
            map: map,
            animation: google.maps.Animation.DROP,
            flat: true,
            draggable: false,
            title: "Luce Balloons",
            icon: myIcon,
            'optimized': false
        });
        map.set('styles', mapstyle);
    }
    $(document).ready(function(){
        initMap();
    });
</script>
@endsection
@section('modals')
@endsection