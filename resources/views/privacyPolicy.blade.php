@php
$page = 'Privacy Policy';
$pagetitle = 'Privacy Policy - Luce Balloons | Balloons for parties, weddings, corporate events & special occasions';
$metadescription = 'Privacy Policy';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative  py-5 mt-5 mob-mt-0 page-top overflow-hidden">
  <div class="row mt-5 justify-content-center position-relative z-2">
    <div class="col-12 mob-px-3 text-left mt-5">
    	<h1 class="text-primary">Privacy Policy</h1>
    	<p>We treat customers’ personal information and details with the utmost sensitivity and confidentiality. We follow strict security procedures in the storage and disclosure of information which you have given us, to prevent unauthorised access in accordance with the UK data protection legislation.</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container position-relative z-2 pb-5 mb-5">
	<div class="row">
		<div class="col-12">
			<p>We do not collect sensitive information about you except when you specifically knowingly provide it.</p>
			<p>In order to maintain the accuracy of our database, you can check, update or remove your personal details by writing to us or sending us an email.<br>
			We do not store credit card details nor do we share customer details with any 3rd parties</p>
			<p><b>Accuracy of Your Data</b><br>
			We endeavour to keep your information accurate and up to date at all times. If you become aware that any information held by us is inaccurate please contact us any time either by telephone, fax, post or e-mail.</p>
			<p><b>Using Your Data</b><br>
			When you place an order with us we will need to know your name, address and other information including e-mail address and telephone number. We store this information for the purposes of providing you with the very best possible service. The relevant information may be used by us, our agents or sub-contractors as a method to support our relationship with you. This includes communicating with you on matters concerning new products, special offers and any other service-related issues.</p>
			<p><b>Using Your Data to Communicate With You</b><br>
			We undertake not to provide you with any communication that you do not require. If you don’t wish to receive communication by post, fax, telephone or email, please inform us by any of the previous methods of your preference to opt out of any of these services.</p>
			<p><b>Cookies</b><br>
			When you visit our website we send your computer a ‘cookie’. Cookies help us to track basic visitor information in order that we can better tailor the site to our visitors’ needs. Most web browsers automatically accept cookies but you can disable this function by changing your browser settings if you wish.<br>
			Cookies are text-only strings of information that a website transfers to the cookie file of your browser on your computer’s hard disk so that the website can remember who you are.<br>
			A cookie will typically contain the name of the domain from which the cookie has come, the “lifetime” of the cookie, and a value. Cookies may be used to compile anonymous, aggregated statistics which provide a better understanding of how a website is being used. You cannot be identified personally in this way.</p>
			<p>You can easily delete any cookies that have been installed in the cookie folder of your browser and you can also stop cookies being stored on your computer in future. In order to do this you should refer to your browser’s instructions by clicking “help” in your browser menu. Further information on deleting or controlling cookies is available.</p>
			<p>We use Google Analytics, also known as a ‘Performance’ cookie to collect information about how you use our website e.g. which pages you visit, and if you experience any errors. These cookies don’t collect any information that could identify you – all the information collected is anonymous and is only used to help us improve how our website works, understand what interests our users and measure how effective our advertising is.</p>
		</div>
	</div>
</div>
@endsection


