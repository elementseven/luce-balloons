@php
$page = 'Blog';
$pagetitle = 'Blog - Luce Balloons';
$metadescription = 'See our latest Blog and advice';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5 mob-pt-0">
    <div class="col-lg-12 mt-5 text-center text-lg-left">
      <h1 class="mt-5 mb-4 text-primary">Balloon Blog</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<blog-index :categories="{{$categories}}"></blog-index>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection