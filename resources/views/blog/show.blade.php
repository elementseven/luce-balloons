@php
$page = 'Blog';
$pagetitle = $post->title . ' | Luce Balloons';
$metadescription = $post->excerpt;
$pagetype = 'light';
$pagename = 'blog';
$ogimage = 'https://luceballoons.co.uk' . $post->getFirstMediaUrl('featured');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5 mob-mb-5">
    <div class="col-xl-12 pt-5 mob-px-4 mb-5 mob-mb-0">
      <p class="pt-5 mob-pt-0 mb-2"><b><a href="{{route('blog')}}"><i class="fa fa-arrow-circle-left"></i>&nbsp; Browse blog</a></b></p>
      <h1 class="text-primary blog-title mb-4">{{$post->title}}</h1>
      <p class="text-larger mt-3">{{$post->excerpt}}</p> 
      <p class="mb-1 text-larger"><b>Share this article:</b></p>
      <p class="text-larger">
          <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
          <i class="fa fa-facebook"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
          <i class="fa fa-linkedin ml-2"></i>
        </a>
        <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
          <i class="fa fa-twitter ml-2"></i>
        </a>
        <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
          <i class="fa fa-whatsapp ml-2"></i>
        </a>
      </p>
      
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4 position-relative z-2">
    <div class="row pb-5 mob-py-0">
        <div class="col-xl-10 mob-mt-0 blog-body">
          <picture> 
            <source  srcset="{{$post->getFirstMediaUrl('blog', 'normal-webp')}}" type="image/webp"/> 
            <source srcset="{{$post->getFirstMediaUrl('blog', 'normal')}}" type="{{$post->getFirstMedia('blog')->mimetype}}"/> 
            <img src="{{$post->getFirstMediaUrl('blog', 'normal')}}" type="{{$post->getFirstMedia('blog')->mimetype}}" alt="{{$post->title}}" class="w-100 mb-5" />
          </picture>
            
          {!!$post->body!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1 text-larger"><b>Share this article:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<div class="container py-5 mob-px-3 position-relative z-2">
    <div class="row">
        <div class="col-12 text-center mb-4">
            <h2 class="mb-4 text-red">More Blog Posts</h2>
        </div>
        @foreach($others as $key => $post)
        <div class="col-md-4 mb-5">
          <a href="{{route('blog-single', ['slug' => $post->slug, 'date' => $post->getDate($post->created_at)])}}">
            <div class="card border-0 shadow overflow-hidden post-box text-center text-md-left text-dark zoom-link">
              <div class="post-image zoom-img">
                <picture> 
                  <source  srcset="{{$post->getFirstMediaUrl('blog', 'featured-webp')}}" type="image/webp"/> 
                  <source srcset="{{$post->getFirstMediaUrl('blog', 'featured')}}" type="{{$post->getFirstMedia('blog')->mimetype}}"/> 
                  <img src="{{$post->getFirstMediaUrl('blog', 'featured')}}" type="{{$post->getFirstMedia('blog')->mimetype}}" alt="{{$post->title}}" class="w-100" />
                </picture>
              </div>
              <div class="p-4">
                <p class="post-exerpt text-small mb-1 text-red">{{$post->getFancyDate($post->created_at)}}</p>
                <h4 class="post-title text-primary mb-2">{{$post->title}}</h4>
                <p class="post-exerpt text-small mb-3">{{substr($post->excerpt,0,100)}}...</p>
                <p class="mb-0 text-red"><b>Read more</b> <i class="fa fa-arrow-circle-right text-red ml-1"></i></p>
              </div>
            </div>
          </a>
        </div>
        @endforeach
    </div>
</div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection