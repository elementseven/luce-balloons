<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #000;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:300; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px 40px 0;">
					<img src="https://luceballoons.co.uk/img/logos/logo.png" width="150" style="margin: auto; display: block;"/>
				</p>

				<p style="font-size:22px; background-color: #1D3D8D; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">New Order</span></p>

				<p style="font-size:18px; color:#000; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi Eric,</span></p>

				<p style="font-size:18px; color:#000; text-align: left; font-family:'Arial', arial, sans-serif;">You have recieved an order on your website, you can view the order details by <a href="https://luceballoons.co.uk/nova/resources/invoices/{{$invoice->id}}" style="color: #1D3D8D;">clicking here</a>.</p>

				</td>
				</tr>
				<tr><td>
				<p style="border-bottom: 1px solid #000; margin: 40px auto;"></p>
				<img src="https://luceballoons.co.uk/img/logos/logo.png" width="150px" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#000; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the <a href="https://luceballoons.co.uk">Luce Balloons Website</a>.<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>