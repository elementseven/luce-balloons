<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:300; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px;"><img src="https://luceballoons.co.uk/img/logos/logo.png" width="250px" alt="Luce Balloons Logo"/></p>

				<p style="font-size:22px; background-color: #1D3D8D; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Receipt</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi {{$user->first_name}},</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Thank you for yor recent purchase. This email acts as your receipt, please keep it for your records. Please check the details of your purchase below.</p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">We will be in touch shortly to make any necessary arrangements.</p>

				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Name:</b> {{$user->full_name}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Payment reference:</b> {{$invoice->transaction_id}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;"><b>Products:</b></p>

				@foreach($invoice->variants as $variant)
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;">{{$variant->product->title}} - {{$variant->title}} - x{{$variant->pivot->qty}}</p>
				@endforeach

				@if(count($invoice->customorders))
        @foreach($invoice->customorders as $key => $co)

        @if($co->decorthrees)
        <p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;">Custom bunch of 3 balloons - £{{$co->price}} - x{{$co->qty}}</p>
        @endif

        @if($co->decorfives)
        <p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;">Custom bunch of 5 balloons - £{{$co->price}} - x{{$co->qty}}</p>
        @endif

        @if($co->bubbleballoons)
        <p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;">Custom bubble balloon - £{{$co->price}} - x{{$co->qty}}</p>
        @endif

        @if($co->foilballoons)
        <p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;">Custom foil balloon - £{{$co->price}} - x{{$co->qty}}</p>
        @endif
        @endforeach
        @endif

				@if($invoice->notes)
          <p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;"><b>Order Notes:</b></p>
          <p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;">{{$invoice->notes}}</p>
        @endif

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Order Value:</b> &pound;{{number_format($invoice->total, 2)}} </p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Shipping:</b> &pound;{{number_format($invoice->shipping, 2)}} </p>

				<p style="font-size:22px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Total:</b> &pound;{{number_format($invoice->total + $invoice->shipping, 2)}} </p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;">You can login to your account by clicking <a href="https://luceballoons.co.uk/login" style="color:#1D3D8D;">this link</a>.</p>

				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
				
				</td>
				</tr>
				<tr><td>
				<img src="https://luceballoons.co.uk/img/logos/logo.png" width="250px" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the <a href="https://luceballoons.co.uk">Luce Balloons website</a><br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>