<p class="mt-8 text-center text-xs text-80">
    <span class="px-1">&middot;</span>Luce Balloons Admin<span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} <a href="https://elementseven.co" class="text-primary dim no-underline" target="_blank">Element Seven</a>
    <span class="px-1">&middot;</span>
</p>
