@php
$page = 'Event Services';
$pagetitle = 'Event Services - Luce Balloons | Balloons for parties, weddings, corporate events & special occasions';
$metadescription = 'Luce Balloons is a professional balloon company that started trading in 1997. We started out as a home based business and have steadily grown into one of the premier balloon companies in Northern Ireland.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid page-top position-relative py-5 overflow-x-hidden">
{{-- 	<img src="/img/shapes/circle-light-blue.svg" class="circle-red" alt="Luce balooons red circle"/>
 --}}	<div class="row py-5 mob-py-0">
		<div class="container pt-5">
		  <div class="row pt-5">
		    <div class="col-lg-10 text-center text-lg-left">
		      <h1 class="mb-4 text-primary mb-4">Event Services</h1>
		      <p class="text-larger mb-4">No matter what your event we can help because we can get or create Balloons for Everything.  Your balloons and decorations can be created using your colour scheme or branding theme.</p>
		      <a href="{{route('contact')}}">
		      	<div class="btn btn-red btn-icon">Contact us <i class="fa fa-chevron-right"></i></div>
		      </a>
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid py-5 mob-pb-0">
  <div class="row py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-5">
          <div class="behind-blue d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/corporate.webp" type="image/webp"/> 
                <source srcset="/img/temp/corporate.jpg" type="image/jpeg"/> 
                <img srcset="/img/temp/corporate.jpg" type="image/jpeg" alt="Balloons for Corporate events in the UK and Ireland" width="314" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-7 pr-5 text-center text-lg-left mob-px-4 position-relative z-2">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-primary mt-4">Corporate</h2>
              <p class="text-large">If you are having a launch event, saying thank you to staff or just want to say “look over here” Luce Balloons can help.</p>
              <a href="{{route('services.corporate')}}">
                <div class="btn btn-primary btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/corporate.svg" width="92" alt="Balloons for corporate events icon" class="smallbouncer" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5 mob-py-0">
  <div class="row py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/wedding.svg" width="128" alt="Balloons for weddings icon" class="smallbouncer" />
            </div>
          </div>
        </div>
        <div class="col-lg-5 pr-5 position-relative z-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-red">Weddings</h2>
              <p class="text-large">Luce Balloons provide high quality balloon décor specialising in amazing effects that will make your day memorable.</p>
              <a href="{{route('services.weddings')}}">
                <div class="btn btn-red btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-5">
          <div class="behind-red d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/weddings.webp" type="image/webp"/> 
                <source srcset="/img/temp/weddings.jpg" type="image/jpeg"/> 
                <img srcset="/img/temp/weddings.jpg" type="image/jpeg" alt="Balloons for weddings in the UK and Ireland" width="314" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-7 pr-5 text-center mob-px-4 position-relative z-2 d-lg-none">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-red mt-4">Weddings</h2>
              <p class="text-large">Luce Balloons provide high quality balloon décor specialising in amazing effects that will make your day memorable.</p>
              <a href="{{route('services.weddings')}}">
                <div class="btn btn-red btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5 mob-py-0">
  <div class="row py-5">
    <div class="container mb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5">
          <div class="behind-yellow d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/parties.webp" type="image/webp"/> 
                <source srcset="/img/temp/parties.jpg" type="image/jpeg"/> 
                <img srcset="/img/temp/parties.jpg" type="image/jpeg" alt="Balloons for parties in the UK and Ireland" width="314" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-6 pr-5 text-center text-lg-left mob-px-5 position-relative z-2">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-yellow mt-4">Parties</h2>
              <p class="text-large">Balloons are perfect for Birthdays, Christenings Anniversaries and any other reason you have to celebrate.</p>
              <a href="{{route('services.parties')}}">
                <div class="btn btn-yellow btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/party.svg" width="105" alt="Balloons for parties icon" class="smallbouncer" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5 mob-py-0">
  <div class="row py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/birthdays.svg" width="128" alt="Balloons for birthdays icon" class="smallbouncer" />
            </div>
          </div>
        </div>
        <div class="col-lg-5 pr-5 position-relative z-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-red">Birthdays</h2>
              <p class="text-large">Birthday balloon decorations can be created to celebrate every birthday especially the big ones.</p>
              <a href="{{route('services.birthdays')}}">
                <div class="btn btn-red btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-5">
          <div class="behind-red d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/birthdays.webp" type="image/webp"/> 
                <source srcset="/img/temp/birthdays.jpg" type="image/jpeg"/> 
                <img srcset="/img/temp/birthdays.jpg" type="image/jpeg" alt="Balloons for birthdays in the UK and Ireland" width="314" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-7 pr-5 text-center mob-px-4 position-relative z-2 d-lg-none">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-red mt-4">Birthdays</h2>
              <p class="text-large">Birthday balloon decorations can be created to celebrate every birthday especially the big ones.</p>
              <a href="{{route('services.birthdays')}}">
                <div class="btn btn-red btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5 mob-py-0">
  <div class="row py-5">
    <div class="container mb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5">
          <div class="behind-yellow d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/baby-events.webp" type="image/webp"/> 
                <source srcset="/img/temp/baby-events.jpg" type="image/jpeg"/> 
                <img srcset="/img/temp/baby-events.jpg" type="image/jpeg" alt="Balloons for baby events in the UK and Ireland" width="314" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-6 pr-5 text-center text-lg-left mob-px-5 position-relative z-2">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-yellow mt-4">Baby Events</h2>
              <p class="text-large">We have balloons, partywear and special effects for Gender Reveal, Baby Shower, New Baby and Baptism / Christening celebrations.</p>
              <a href="{{route('services.babyEvents')}}">
                <div class="btn btn-yellow btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/baby.svg" width="105" alt="Balloons for parties icon" class="smallbouncer" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5 mob-py-0 position-relative z-2">
  <div class="row py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <img src="/img/shapes/weddings.svg" width="128" alt="Balloons for birthdays icon" class="smallbouncer" />
            </div>
          </div>
        </div>
        <div class="col-lg-5 pr-5 position-relative z-2 d-none d-lg-block">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-red">Engagement</h2>
              <p class="text-large">If it’s a small event to mark the occasion or a big party to tell the world we can help with beautiful balloon decorations to suit all budgets.</p>
              <a href="{{route('services.engagement')}}">
                <div class="btn btn-red btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-5">
          <div class="behind-red d-block d-lg-inline-block text-center">
            <div class="rounded-image d-inline-block shadow">
              <picture> 
                <source  srcset="/img/temp/engagement.webp" type="image/webp"/> 
                <source srcset="/img/temp/engagement.jpg" type="image/jpeg"/> 
                <img srcset="/img/temp/engagement.jpg" type="image/jpeg" alt="Balloons for engagement in the UK and Ireland" width="314" />
              </picture>
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-7 pr-5 text-center mob-px-4 position-relative z-2 d-lg-none">
          <div class="d-table w-100 h-100">
           <div class="d-table-cell align-middle w-100 h-100">
              <h2 class="text-red mt-4">Engagement</h2>
              <p class="text-large">If it’s a small event to mark the occasion or a big party to tell the world we can help with beautiful balloon decorations to suit all budgets.</p>
              <a href="{{route('services.engagement')}}">
                <div class="btn btn-red btn-icon">Learn More <i class="fa fa-chevron-right"></i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection