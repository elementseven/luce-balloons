@php
$page = 'Party Services';
$pagetitle = 'Party Services - Luce Balloons | Balloons for Parties in Northern Ireland & Ireland';
$metadescription = 'Luce Balloons is a professional balloon company that started trading in 1997. We started out as a home based business and have steadily grown into one of the premier balloon companies in Northern Ireland.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid page-top position-relative py-5 overflow-x-hidden">
	<img src="/img/shapes/circle-yellow.svg" class="circle-red" alt="Luce balooons red circle"/>
	<div class="row py-5">
		<div class="container pt-5 mob-py-0">
		  <div class="row pt-5">
		    <div class="col-lg-10 text-center text-lg-left">
		      <h1 class="mb-4 text-primary mb-4">Party Services</h1>
		      <p class="text-larger mb-4">Balloons are perfect for Birthdays, Christenings Anniversaries and any other reason you have to celebrate.  Balloons can transform any room from just a space to a place full of great atmosphere.</p>
		      <a href="{{route('contact')}}">
		      	<div class="btn btn-yellow btn-icon">Contact us <i class="fa fa-chevron-right"></i></div>
		      </a>
		    </div>
		  </div>
		</div>
	</div>
  <div class="row">
    <div class="container py-5 mb-5">
      <div class="row">
        <div class="col-lg-5 mob-px-4">
          <picture> 
            <source  srcset="/img/temp/parties2.jpg" type="image/webp"/> 
            <source srcset="/img/temp/parties2.jpg" type="image/jpeg"/> 
            <img src="/img/temp/parties2.jpg" type="image/jpeg" alt="Party Services - Luce Balloons Northern Ireland" class="w-100 rounded-image shadow" />
          </picture>
        </div>
        <div class="col-lg-7 pl-5 mob-px-4 mob-mt-4 text-center text-lg-left">
          <h2 class="text-primary mb-4">Parties in N.Ireland</h2>
          <p class="text-large">With so much to choose from we can help create balloon decorations for everything using our pre-printed ranges or something crafted just for you. Everything will work well in small private parties at home or in large function rooms.  We can also deliver your arrangements to your venue.</p>
          <p class="text-large mb-4">Our balloon and party ranges start from new baby to retirement and beyond with arrangements to mark every occasion like passing a driving test, graduation and new home to pick just 3.</p>
          <a href="{{route('contact')}}">
            <div class="btn btn-yellow btn-icon">Contact us <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<popular-products :category="'*'" class="pb-5"></popular-products>
<div class="container-fluid balloons-bg bg bg-fixed position-relative mt-5">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container py-5">
      <div class="row py-5">
        <div class="col-12 text-center">
          <h2 class="text-primary">Customise Balloons</h2>
          <p class="text-primary"><b>Looking for something unique, with customised writing, colours and shapes?</b></p>
          <a href="#">
            <div class="btn btn-yellow btn-icon">Start Customising <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5 mob-px-4 position-relative z-2">
  <div class="row pt-5 mob-py-3">
    <div class="col-12 text-center mb-4">
      <h2 class="mb-4 text-primary">Balloon Blog</h2>
    </div>
    @foreach($posts as $post)
    <div class="col-md-4 mb-5">
      <a href="{{route('blog-single', ['slug' => $post->slug, 'date' => $post->getDate($post->created_at)])}}">
        <div class="card border-0 shadow overflow-hidden post-box text-center text-md-left text-dark zoom-link">
          <div class="post-image zoom-img">
            <picture> 
              <source  srcset="{{$post->getFirstMediaUrl('blog', 'featured-webp')}}" type="image/webp"/> 
              <source srcset="{{$post->getFirstMediaUrl('blog', 'featured')}}" type="{{$post->getFirstMedia('blog')->mimetype}}"/> 
              <img src="{{$post->getFirstMediaUrl('blog', 'featured')}}" type="{{$post->getFirstMedia('blog')->mimetype}}" alt="{{$post->title}}" class="w-100" />
            </picture>
          </div>
          <div class="p-4">
            <p class="post-exerpt text-small mb-1 text-red">{{$post->getFancyDate($post->created_at)}}</p>
            <h4 class="post-title text-primary mb-2">{{$post->title}}</h4>
            <p class="post-exerpt text-small mb-3">{{substr($post->excerpt,0,100)}}...</p>
            <p class="mb-0 text-red"><b>Read more</b> <i class="fa fa-arrow-circle-right text-red ml-1"></i></p>
          </div>
        </div>
      </a>
    </div>
    @endforeach
    <div class="col-12 text-center">
      <a href="{{route('blog')}}">
        <div class="btn btn-red btn-icon">All blog posts <i class="fa fa-chevron-right"></i></div>
      </a>
    </div>
  </div>
</div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection