@php
$page = 'Baby Events';
$pagetitle = 'Baby Events - Luce Balloons | Balloons for Baby Events in Northern Ireland & Ireland';
$metadescription = 'Luce Balloons is a professional balloon company that started trading in 1997. We started out as a home based business and have steadily grown into one of the premier balloon companies in Northern Ireland.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://luceballoons.co.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid page-top position-relative py-5 overflow-x-hidden">
	<img src="/img/shapes/circle-yellow.svg" class="circle-red" alt="Luce balooons red circle"/>
	<div class="row py-5">
		<div class="container pt-5 mob-py-0">
		  <div class="row pt-5">
		    <div class="col-lg-10 text-center text-lg-left">
		      <h1 class="mb-4 text-primary mb-4">Baby Events</h1>
		      <p class="text-larger mb-4">We have balloons, partywear and special effects for Gender Reveal, Baby Shower, New Baby and Baptism / Christening celebrations.  The party size doesn’t matter as we can put packages together to suit all.</p>
		      <a href="{{route('contact')}}">
		      	<div class="btn btn-yellow btn-icon">Contact us <i class="fa fa-chevron-right"></i></div>
		      </a>
		    </div>
		  </div>
		</div>
	</div>
  <div class="row">
    <div class="container py-5 mb-5">
      <div class="row justify-content-center">
        <div class="col-lg-10 mob-px-4 mob-mt-4 text-center">
          <h2 class="text-primary mb-3">Baby Events in N.Ireland</h2>
          <p class="text-large">The 4 main events in the Baby group would be the Gender Reveal party, The Baby Shower, The Arrival and the Christening / Naming Ceremony.</p>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-lg-6 mb-5 text-center">
          <picture> 
            <source  srcset="/img/temp/gender-reveal.jpg" type="image/webp"/> 
            <source srcset="/img/temp/gender-reveal.jpg" type="image/jpeg"/> 
            <img src="/img/temp/gender-reveal.jpg" type="image/jpeg" alt="Baby Events - Gender Reveal - Luce Balloons Northern Ireland" class="w-100 rounded-image shadow" />
          </picture>
          <h3 class="mb-3 mt-4 text-primary bigger">Gender reveal</h3>
          <p class="text-large mb-4">Gender reveal parties are becoming ever more popular and we can help break the news with a bang when you pop the Gender Reveal balloon and shower the area with Pink or Blue confetti.  We also do a version that can have lots of small balloons fill your ceiling after the big bang.  We can supply Gender Reveal Cannons that with a twist fill your area with Pink or Blue confetti.</p>
        </div>
        <div class="col-lg-6 mb-5 text-center">
          <picture> 
            <source  srcset="/img/temp/baby-shower.jpg" type="image/webp"/> 
            <source srcset="/img/temp/baby-shower.jpg" type="image/jpeg"/> 
            <img src="/img/temp/baby-shower.jpg" type="image/jpeg" alt="Baby Events - Baby Shower - Luce Balloons Northern Ireland" class="w-100 rounded-image shadow" />
          </picture>
          <h3 class="mb-3 mt-4 text-primary bigger">Baby Shower</h3>
          <p class="text-large mb-4">Baby Showers are a good excuse to have a party and make a fuss of the “Mother to Be”. The shower part is when you shower her with gifts and words of encouragement while having fun.  To help with this we have different ranges and themes of Baby Shower balloons, banners and partywear.</p>
        </div>
        <div class="col-lg-6 mb-5 text-center">
          <picture> 
            <source  srcset="/img/temp/arrival.jpg" type="image/webp"/> 
            <source srcset="/img/temp/arrival.jpg" type="image/jpeg"/> 
            <img src="/img/temp/arrival.jpg" type="image/jpeg" alt="Baby Events - Arrival - Luce Balloons Northern Ireland" class="w-100 rounded-image shadow" />
          </picture>
          <h3 class="mb-3 mt-4 text-primary bigger">The Arrival</h3>
          <p class="text-large mb-4">We have lots of Welcome New Baby balloon arrangements to choose from in our Baby Girl, Baby Boy and Twins ranges.</p>
        </div>
        <div class="col-lg-6 mb-5 text-center">
          <picture> 
            <source  srcset="/img/temp/christening.jpg" type="image/webp"/> 
            <source srcset="/img/temp/christening.jpg" type="image/jpeg"/> 
            <img src="/img/temp/christening.jpg" type="image/jpeg" alt="Baby Events - Christening - Luce Balloons Northern Ireland" class="w-100 rounded-image shadow" />
          </picture>
          <h3 class="mb-3 mt-4 text-primary bigger">Christening / Naming Ceremony</h3>
          <p class="text-large mb-4">We have beautiful Christening and Baptism ranges to choose from which can be complimented with our personalised balloons and arranged into arches and table décor at home or a function room.</p>
        </div>
        <div class="col-12 text-center">
          <a href="{{route('contact')}}">
            <div class="btn btn-yellow btn-icon">Contact us <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<popular-products :category="'*'" class="pb-5"></popular-products>
<div class="container-fluid balloons-bg bg bg-fixed position-relative mt-5">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container py-5">
      <div class="row py-5">
        <div class="col-12 text-center">
          <h2 class="text-primary">Customise Balloons</h2>
          <p class="text-primary"><b>Looking for something unique, with customised writing, colours and shapes?</b></p>
          <a href="#">
            <div class="btn btn-yellow btn-icon">Start Customising <i class="fa fa-chevron-right"></i></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5 mob-px-4 position-relative z-2">
  <div class="row pt-5 mob-py-3">
    <div class="col-12 text-center mb-4">
      <h2 class="mb-4 text-primary">Balloon Blog</h2>
    </div>
    @foreach($posts as $post)
    <div class="col-md-4 mb-5">
      <a href="{{route('blog-single', ['slug' => $post->slug, 'date' => $post->getDate($post->created_at)])}}">
        <div class="card border-0 shadow overflow-hidden post-box text-center text-md-left text-dark zoom-link">
          <div class="post-image zoom-img">
            <picture> 
              <source  srcset="{{$post->getFirstMediaUrl('blog', 'featured-webp')}}" type="image/webp"/> 
              <source srcset="{{$post->getFirstMediaUrl('blog', 'featured')}}" type="{{$post->getFirstMedia('blog')->mimetype}}"/> 
              <img src="{{$post->getFirstMediaUrl('blog', 'featured')}}" type="{{$post->getFirstMedia('blog')->mimetype}}" alt="{{$post->title}}" class="w-100" />
            </picture>
          </div>
          <div class="p-4">
            <p class="post-exerpt text-small mb-1 text-red">{{$post->getFancyDate($post->created_at)}}</p>
            <h4 class="post-title text-primary mb-2">{{$post->title}}</h4>
            <p class="post-exerpt text-small mb-3">{{substr($post->excerpt,0,100)}}...</p>
            <p class="mb-0 text-red"><b>Read more</b> <i class="fa fa-arrow-circle-right text-red ml-1"></i></p>
          </div>
        </div>
      </a>
    </div>
    @endforeach
    <div class="col-12 text-center">
      <a href="{{route('blog')}}">
        <div class="btn btn-red btn-icon">All blog posts <i class="fa fa-chevron-right"></i></div>
      </a>
    </div>
  </div>
</div>
<div class="container py-5 text-center position-relative z-2 mb-5">
  <h2 class="text-primary mb-2">Sign up to our mailing list</h2>
  <a href="https://pageseu.actmkt.com/l/Yf4y7D3FwZx95Pcs6RXg" target="_blank">
    <button type="button" class="btn btn-red">Sign Up</button>
  </a>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')

@endsection