/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("babel-polyfill");
require("whatwg-fetch");
require('./bootstrap');
window.LazyLoad = require('vanilla-lazyload');
import Vue from 'vue';

window.moment = require('moment');
import vueDebounce from 'vue-debounce';
require('./plugins/modernizr-custom.js');
require('./plugins/cookieConsent.js');
require('waypoints/lib/jquery.waypoints.min.js');

import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';

require('./a-general.js');

Vue.use(vueDebounce, {
  listenTo: ['input', 'keyup']
});
Vue.use(Datetime);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('mailing-list', require('./components/MailingList.vue').default);

Vue.component('blog-index', require('./components/blog/Index.vue').default);
Vue.component('blog-show', require('./components/blog/Show.vue').default);

// Shop components
Vue.component('shop-index', require('./components/Shop/Index.vue').default);
Vue.component('shop-show', require('./components/Shop/Show.vue').default);
Vue.component('show-product', require('./components/Shop/Product.vue').default);
Vue.component('popular-products', require('./components/Shop/PopularProducts.vue').default);

Vue.component('basket', require('./components/Shop/Basket.vue').default);
Vue.component('update-quantity', require('./components/Shop/UpdateQuantity.vue').default);
Vue.component('confirm-add-to-basket', require('./components/Shop/ConfirmAddToBasket.vue').default);

Vue.component('contact-page-form', require('./components/ContactPageForm.vue').default);
Vue.component('countries', require('./components/Countries.vue').default);
Vue.component('checkout', require('./components/Checkout/Index.vue').default);
Vue.component('personal-details', require('./components/Checkout/PersonalDetails.vue').default);
Vue.component('address-details', require('./components/Checkout/Address.vue').default);
Vue.component('payment-details', require('./components/Checkout/Payment.vue').default);
Vue.component('shipping-details', require('./components/Checkout/Shipping.vue').default);
Vue.component('stages-bar', require('./components/Checkout/Stages.vue').default);

Vue.component('custom-balloons', require('./components/CustomBalloons/index.vue').default);
Vue.component('bubble-balloons', require('./components/CustomBalloons/BubbleBalloons.vue').default);

Vue.component('gallery', require('./components/Gallery.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
