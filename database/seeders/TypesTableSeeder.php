<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('types')->insert([
        'title' => 'Balloons',
        'slug' => 'balloons',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
	    DB::table('types')->insert([
        'title' => 'Banners',
	    	'slug' => 'banners',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
	    DB::table('types')->insert([
        'title' => 'Cups',
	    	'slug' => 'cups',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
	    DB::table('types')->insert([
        'title' => 'Helium',
	    	'slug' => 'helium',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
	    DB::table('types')->insert([
        'title' => 'Napkins',
	    	'slug' => 'napkins',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
	    DB::table('types')->insert([
        'title' => 'Party Packs',
	    	'slug' => 'party-packs',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
	    DB::table('types')->insert([
        'title' => 'Plates',
	    	'slug' => 'plates',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
	    DB::table('types')->insert([
        'title' => 'Table Covers',
	    	'slug' => 'table-covers',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
	    DB::table('types')->insert([
        'title' => 'Wrapping Paper',
        'slug' => 'wrapping-paper',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
	    ]);
    }
}
