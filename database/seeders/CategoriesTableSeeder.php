<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('categories')->insert([
        'title' => 'Baby Events',
        'slug' => 'baby-events',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
      ]);
      DB::table('categories')->insert([
        'title' => 'Birthdays',
        'slug' => 'birthdays',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
      ]);
      DB::table('categories')->insert([
        'title' => 'Corporate',
        'slug' => 'corporate',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
      ]);
      DB::table('categories')->insert([
        'title' => 'Engagement',
        'slug' => 'engagement',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
      ]);
      DB::table('categories')->insert([
        'title' => 'Parties',
        'slug' => 'parties',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
      ]);
      DB::table('categories')->insert([
        'title' => 'Weddings',
        'slug' => 'weddings',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
      ]);
      DB::table('categories')->insert([
        'title' => 'Other',
        'slug' => 'other',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
      ]);
    }
}
