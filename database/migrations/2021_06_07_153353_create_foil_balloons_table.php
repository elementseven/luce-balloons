<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoilBalloonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foil_balloons', function (Blueprint $table) {
            $table->id();
            $table->string('colour');
            $table->string('shape');
            $table->string('textcolour');
            $table->string('font');
            $table->string('text');
            $table->string('extras');
            $table->text('notes')->nullable();
            $table->integer('quantity');
            $table->decimal('price');

            $table->bigInteger('customorder_id')->unsigned()->index()->nullable();
            $table->foreign('customorder_id')->references('id')->on('custom_orders')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foil_balloons');
    }
}
