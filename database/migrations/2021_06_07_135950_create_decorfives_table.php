<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecorfivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decorfives', function (Blueprint $table) {
            $table->id();
            $table->string('balloon1');
            $table->string('balloon2');
            $table->string('balloon3');
            $table->string('balloon4');
            $table->string('balloon5');
            $table->string('weight');
            $table->text('notes')->nullable();
            $table->integer('quantity');
            $table->decimal('price');

            $table->bigInteger('customorder_id')->unsigned()->index()->nullable();
            $table->foreign('customorder_id')->references('id')->on('custom_orders')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decorfives');
    }
}
