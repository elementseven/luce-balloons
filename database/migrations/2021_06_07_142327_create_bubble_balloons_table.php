<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBubbleBalloonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bubble_balloons', function (Blueprint $table) {
            $table->id();
            $table->string('balloon1');
            $table->string('balloon2');
            $table->string('textcolour');
            $table->string('font');
            $table->string('text');
            $table->string('weight');
            $table->string('size');
            $table->text('notes')->nullable();
            $table->integer('quantity');
            $table->decimal('price');

            $table->bigInteger('customorder_id')->unsigned()->index()->nullable();
            $table->foreign('customorder_id')->references('id')->on('custom_orders')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bubble_balloons');
    }
}