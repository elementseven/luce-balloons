<?php

namespace App\Http\Controllers;

use App\Models\FoilBalloon;
use App\Models\CustomOrder;
use Illuminate\Http\Request;
use Cart;

class FoilBalloonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
          'colour' => 'required|string|max:255',
          'shape' => 'required|string|max:255',
          'textcolour' => 'required|string|max:255',
          'font' => 'required|string|max:255',
          'text' => 'required|string|max:255',
          'extras' => 'required|string|max:255',
          'price' => 'required',
          'total' => 'required',
          'quantity' => 'required'
        ]);

        $customorder = CustomOrder::create([
          'price' => $request->input('total')
        ]);

        $foilballoon = FoilBalloon::create([
            'colour' => $request->input('colour'),
            'shape' => $request->input('shape'),
            'textcolour' => $request->input('textcolour'),
            'font' => $request->input('font'),
            'text' => $request->input('text'),
            'extras' => $request->input('extras'),
            'price' => $request->input('price'),
            'notes' => $request->input('notes'),
            'quantity' => $request->input('quantity'),
            'customorder_id' => $customorder->id
        ]);


        // Add the item to the cart
        Cart::add(['id' => $customorder->id, 'name' => 'Custom Foil balloon', 'qty' => $foilballoon->quantity, 'price' => $foilballoon->price, 'weight' => 0, 'options' => ['image'=>'/img/customballoons/foil.jpg', 'original_price'=> $foilballoon->price, 'sale_price'=> null, 'product_title' => 'Foil balloon', 'product_excerpt' => 'Create your own foil balloon!', 'inflated' => true, 'collectiondate' => null, 'shippingtype' => null, 'shipping' => 0.00]])->associate('App\Models\CustomOrder');

        return response()->json(['success' => 'success'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FoilBalloon  $foilBalloon
     * @return \Illuminate\Http\Response
     */
    public function show(FoilBalloon $foilBalloon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FoilBalloon  $foilBalloon
     * @return \Illuminate\Http\Response
     */
    public function edit(FoilBalloon $foilBalloon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FoilBalloon  $foilBalloon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoilBalloon $foilBalloon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FoilBalloon  $foilBalloon
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoilBalloon $foilBalloon)
    {
        //
    }
}
