<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\CustomOrder;
use App\Models\Category;
use App\Models\Variant;
use App\Models\Product;
use App\Models\Address;
use App\Models\Invoice;
use App\Models\Type;
use App\Models\User;

use Cart;
use Auth;
use Hash;
use Mail;
use Session;

class ShopController extends Controller
{

  // Get balloons for the custom ballon shop section
  public function getCustomBalloons(Request $request){

    $products = Product::where('status', '!=', 'Unavailable')
    ->whereHas('types', function($q) use($request){
      $q->whereIn('id', [1,4]);
    })
    ->whereDoesntHave('types', function($q) use($request){
      $q->whereIn('id', [11]);
    })
    ->whereHas('variants')
    ->with(array('variants' => function($query) {
      return $query->where('status', 'Available')->orderBy('price', 'asc')->select('id','price','sale_price','title','product_id');
    }))
    ->orderBy('title','desc')
    ->get(['id','title','slug']);

    // Return images with products
    foreach($products as $p){
      $p->price = $p->variants[0]->price;
      $p->mimetype = $p->getFirstMedia('featured')->mime_type;
      $p->featured = $p->getFirstMediaUrl('featured', 'thumb');
      foreach($p->variants as $v){
        $v->mimetype = $v->getFirstMedia('featured')->mime_type;
        $v->featured = $v->getFirstMediaUrl('featured', 'thumb');
      }
    }
    return $products;

  }

  // Function to create a user
  public function createUser(Request $request){

    $this->validate($request,[
      'first_name' => 'required|string|max:255',
      'last_name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:8|confirmed',
      'consent' => 'accepted'
    ]);
    $phone = null;
    if($request->input('phone')){
      $phone = $request->input('phone');
    }
    $user = User::create([
      'first_name' => $request->input('first_name'),
      'last_name' => $request->input('last_name'),
      'email' => $request->input('email'),
      'phone' => $phone,
      'password' => Hash::make($request->input('password')),
      'role_id' => 2
    ]);

    Auth::login($user);

      //Send the user login details for their account
    $this->accountCreated($user);

    return $user;

  }

  /**
   * Set the shipping rates for an order.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function setShipping(Request $request)
  {

    // Validate the form data
    $this->validate($request,[
      'shippingtype' => 'required|string|max:255'
    ]);

    $shipping = 0.00;
    // if($request->input('shippingtype') == "Standard delivery"){
    //   $shipping = 5.00;
    // }else if($request->input('shippingtype') == "Next day delivery"){
    //   $shipping = 15.00;
    // }

    foreach(Cart::content() as $cc){
      $rowid = $cc->rowId;
      $options = $cc->options;
      $options['shippingtype'] = $request->input('shippingtype');
      if($request->input('shippingtype') == "Click and collect"){
        $options['collectiondate'] = Carbon::parse($request->input('collectiondate'))->format('Y-m-d');
      }
      $options['shipping'] = $shipping;
      $cart = Cart::update($rowid, array('options' => $options));
    }

    return $shipping;
  }

  /**
   * Store a newly created address in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function storeAddress(Request $request)
  {
    if($request->input('same') == 1){
      // Validate the form data
      $this->validate($request,[
        'streetAddress' => 'required|string|max:255',
        'city' => 'required|string|max:255',
        'postalCode' => 'required|string|max:255',
        'country' => 'required|string|max:255'
      ]);
    }else{
      // Validate the form data
      $this->validate($request,[
        'streetAddress' => 'required|string|max:255',
        'city' => 'required|string|max:255',
        'postalCode' => 'required|string|max:255',
        'country' => 'required|string|max:255',
        'shippingstreetAddress' => 'required|string|max:255',
        'shippingcity' => 'required|string|max:255',
        'shippingpostalCode' => 'required|string|max:255',
        'shippingcountry' => 'required|string|max:255'
      ]);
    }

    // Store the user's address(es)
    $billingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), 'billing');

    if($request->input('same') == 1){
      $shippingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), 'shipping');
    }else{
      $shippingAddress = $this->createAddress($request->input('shippingstreetAddress'), $request->input('shippingextendedAddress'),$request->input('shippingcity'), $request->input('shippingpostalCode'), $request->input('shippingcountry'), $request->input('shippingregion'), 'shipping');
    }

    return $billingAddress;
  }



  /**
   * Store a newly created Invoice in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function payment(Request $request)
  {
    $user = Auth::user();
    $eventdate = null;
    $collectiondate = null;

    if($request->input('collectiondate') != null && $request->input('collectiondate') != 'null'){
      $collectiondate = Carbon::parse($request->input('collectiondate'))->format('Y-m-d H:i:s');
    }
    
    if($request->input('eventdate') != null && $request->input('eventdate') != 'null'){
      $eventdate = Carbon::parse($request->input('eventdate'))->format('Y-m-d H:i:s');
    }

    $invoice = Invoice::create([
       'transaction_id' => $request->input('paymentId'),
       'notes' => $request->input('notes'),
       'total' => Cart::total(2,'.',''),
       'shipping' => $request->input('shipping'),
       'shipping_type' => $request->input('shippingtype'),
       'collection_date' => $collectiondate,
       'event_date' => $eventdate,
       'user_id' => $user->id
     ]);

    foreach(Cart::content() as $cc){
      if($cc->name == "Custom bunch of 3 balloons"){
        $customorder = CustomOrder::where('id', $cc->model->id)->first();
        $customorder->invoice_id = $invoice->id;
        $customorder->qty = $cc->qty;
        $customorder->user_id = $user->id;
        $customorder->save();
      }else if($cc->name == "Custom bunch of 5 balloons"){
        $customorder = CustomOrder::where('id', $cc->model->id)->first();
        $customorder->invoice_id = $invoice->id;
        $customorder->qty = $cc->qty;
        $customorder->user_id = $user->id;
        $customorder->save();
      }else if($cc->name == "Custom Foil balloon"){
        $customorder = CustomOrder::where('id', $cc->model->id)->first();
        $customorder->invoice_id = $invoice->id;
        $customorder->qty = $cc->qty;
        $customorder->user_id = $user->id;
        $customorder->save();
      }else if($cc->name == "Custom Bubble balloon"){
        $customorder = CustomOrder::where('id', $cc->model->id)->first();
        $customorder->invoice_id = $invoice->id;
        $customorder->qty = $cc->qty;
        $customorder->user_id = $user->id;
        $customorder->save();
      }else{
        $invoice->variants()->attach($cc->model->id, ['qty' => $cc->qty]);
      }
    }

    // Sign the new customer up to a mailing list
    // if($request->input('mailchimp') == 1){
    //  Newsletter::subscribe($user->email, ['FNAME'=>$user->first_name, 'LNAME'=>$user->last_name]);
    // }

    // Send confirmation emails
    $this->paymentConfirmed($invoice, $user);
    $this->newOrder($invoice);
    Cart::destroy();
    return $invoice;
  }

  // Get payment intent for Stripe
  public function getIntent(){
    \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
    $shipping = 0.00;
    foreach(Cart::content() as $cc){
      $shipping = $cc->options->shipping;
    }
    $total = (Cart::total(2,'.','') + $shipping) * 100;
    $intent = \Stripe\PaymentIntent::create([
      'amount' => $total,
      'currency' => 'gbp',
      // Verify your integration in this guide by including this parameter
      'metadata' => ['integration_check' => 'accept_a_payment'],
    ]);
    return $intent;
  }

  // Send payment confirmed email
  public function paymentConfirmed($invoice, $user){

    Mail::send('emails.Receipt',[
     'user' => $user,
     'invoice' => $invoice
   ], function ($message) use ($invoice, $user)
   {
      $message->from('donotreply@luceballoons.co.uk', 'Luce Balloons');
      $message->subject('Receipt - Luce Balloons');
      $message->to($user->email);
      // $message->to('luke@elementseven.co');
   });

    return "success";
  }

  // Send Account Created email
  private function accountCreated($user)
  { 
    Mail::send('emails.AccountCreated',[
      'user' => $user
    ], function ($message) use ($user)
    {
      $message->from('donotreply@luceballoons.co.uk', 'Luce Balloons');
      $message->subject('Account Created');
      $message->to($user->email);
      // $message->to('luke@elementseven.co');
    });
    return true;
  }

  // Send Account Created email
  private function newOrder($invoice)
  { 
    Mail::send('emails.NewOrder',[
      'invoice' => $invoice
    ], function ($message) use ($invoice)
    {
      $message->from('donotreply@luceballoons.co.uk', 'Luce Balloons');
      $message->subject('New Order');
      $message->to('eric@luceballoons.co.uk');
      // $message->to('luke@elementseven.co');
    });
    return true;
  }

  // Add a product to the basket
  public function addToBasket(Request $request, Product $product, Variant $variant, $qty){

    // Make sure quantity is an integer
    $qty = intval($qty);

    // Determine if item is on sale and adjust price accordingly
    $price = $variant->price;
    if($variant->sale_price){
      $price = $variant->sale_price;
    }

    // Determine the right image for the item
    if($variant->hasMedia('featured')){
      $image = $variant->getFirstMediaUrl('featured','thumb');
    }else{
      $image = $product->getFirstMediaUrl('featured','thumb');
    }

    foreach($variant->product->types as $type){
      $inflated = false;
      if($type->title == 'Inflated Balloons'){
        $inflated = true;
      }
    }
    // Add the item to the cart
    Cart::add(['id' => $variant->id, 'name' => $variant->title, 'qty' => $qty, 'price' => $price, 'weight' => 0, 'options' => ['image'=>$image, 'original_price'=> $variant->price, 'sale_price'=> $variant->sale_price, 'product_title' => $product->title, 'product_excerpt' => $product->excerpt, 'inflated' => $inflated, 'collectiondate' => null, 'shippingtype' => null, 'shipping' => 0.00]])->associate('App\Models\Variant');

    // Set the tax rate for each cart item to 0
    $items = Cart::content();
    $tax_rate = 0;
    config( ['cart.tax' => $tax_rate] );
    foreach ($items as $item){
      $item->setTaxRate($tax_rate);
      Cart::update($item->rowId, $item->qty); 
    }

    return redirect()->to('/basket');
  }

  // Basket page
  public function basket(){
    return view('shop.basket');
  }

  // Empty the basket
  public function emptyBasket(){
    Cart::destroy();
    return redirect()->to("/basket");
  }

  // Remove a row from cart
  public function remove($rowId){
    Cart::remove($rowId);
    if(Cart::count() == 0){
     Cart::destroy();
   }
   return redirect()->to("/basket");
  }

  // Update Quantity of item in cart
  public function updateQuantity($rowId, $qty){
    if($qty != 0){
      Cart::update($rowId, $qty);
    }else{
      $this->remove($rowId);
    }
    return redirect()->to("/basket");
  }

  // Display success page
  public function success($transaction_id){
    $invoice = Invoice::where('transaction_id', $transaction_id)->first();
    return view('shop.success')->with(['invoice' => $invoice]);
  }

  // Function to create a new invoice and add it to the user
  public function createInvoice($transaction_id, $total, $shipping){
    $invoice = Invoice::create([
     'transaction_id'=> $transaction_id,
     'total'=> $total,
     'shipping' => $shipping
   ]);
    return $invoice;
  }

  // Update the invoice total
  public function updateTotal(Invoice $invoice){
    $invoice->total = number_format((float)Cart::total(), 2, '.', '');
    $invoice->save();
  }


  // Display checkout page
  public function checkout(){
    $billingaddress = null;
    $shippingaddress = null;
    $inflated = 0;

    if(Auth::check() && count(Auth::user()->addresses)){
      $billingaddress = Address::where([['user_id', '=', Auth::id()], ['type', '=', 'billing']])->first();
      $shippingaddress = Address::where([['user_id', '=', Auth::id()], ['type', '=', 'shipping']])->first();
    }
    $shipping = 0;
    $counter = 0;
    foreach(Cart::content() as $cc){
      if($cc->options->inflated){
        $counter++;
      }
    }
    if($counter > 0){
      $inflated = 1;
    }
    return view('shop.checkout')->with(['inflated' => $inflated, 'shipping' => $shipping, 'billingaddress' => $billingaddress, 'shippingaddress' => $shippingaddress]);
  }

  // Shop index page
  public function shop(){
    $categories = Category::has('products')->orderBy('title','asc')->get();
    $types = Type::has('products')->orderBy('title','asc')->get();
    return view('shop.index', compact('categories','types'));
  }

  // Show individual product page
  public function productShow($slug){

  	$product = Product::where('slug', $slug)->where('status', '!=', 'draft')->with('variants')->first();
  	$product->featured = $product->getFirstMediaUrl('featured', 'normal');
  	$product->featured_webp = $product->getFirstMediaUrl('featured', 'normal-webp');
  	$product->mimetype = $product->getFirstMedia('featured')->mime_type;

  	// Slideshow images images
  	$product->slideshow = $product->getMedia('slideshow_images');
    foreach($product->slideshow as $s){
  		$s->featured = $s->getUrl('normal');
      $s->featuredwebp = $s->getUrl('normal-webp');
      $s->mimetype = $s->mime_type;
    }

  	// Images with product variants
    foreach($product->variants as $v){
    	if($v->hasMedia('featured')){
    		$v->featured = $v->getFirstMediaUrl('featured', 'normal');
	      $v->featuredwebp = $v->getFirstMediaUrl('featured', 'normal-webp');
	      $v->mimetype = $v->getFirstMedia('featured')->mime_type;
    	} 
    }

    // return $product;
  	return view('shop.show')->with(['product' => $product]);

  }

  // Return json of products
  public function getProducts(Request $request){

    $products = Product::where('status', '!=', 'Unavailable')
    ->whereHas('categories', function($q) use($request){
      if($request->input('category') != '*'){
        $q->where('id', $request->input('category'));
      }
    })
    ->whereHas('types', function($q) use($request){
      if($request->input('type') != '*'){
        $q->where('id', $request->input('type'));
      }
    })
    ->where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
    ->has('variants')
    ->with(array('variants' => function($query) {
      return $query->orderBy('price', 'asc')->select('id','price','sale_price','product_id');
    }))
    ->orderBy('title','desc')
    ->paginate($request->input('limit'), ['id','title','slug']);

    // Return images with products
    foreach($products as $p){
      $p->featured = $p->getFirstMediaUrl('featured', 'square');
      $p->featuredwebp = $p->getFirstMediaUrl('featured', 'square-webp');
      $p->mimetype = $p->getFirstMedia('featured')->mime_type;
      $p->price = $p->variants[0]->price;
    }
    return $products;
  }

  // Return json of products
  public function getPopularProducts(Request $request){

    $products = Product::where('status', 'Featured')
    ->whereHas('categories', function($q) use($request){
      if($request->input('category') != '*'){
        $q->where('id', $request->input('category'));
      }
    })
    ->whereHas('types', function($q) use($request){
      if($request->input('type') != '*'){
        $q->where('id', $request->input('type'));
      }
    })
    ->where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
    ->has('variants')
    ->with(array('variants' => function($query) {
      return $query->orderBy('price', 'asc')->select('id','price','sale_price','product_id');
    }))
    ->orderBy('title','desc')
    ->paginate($request->input('limit'), ['id','title','slug']);

    // Return images with products
    foreach($products as $p){
      $p->featured = $p->getFirstMediaUrl('featured', 'square');
      $p->featuredwebp = $p->getFirstMediaUrl('featured', 'square-webp');
      $p->mimetype = $p->getFirstMedia('featured')->mime_type;
      $p->price = $p->variants[0]->price;
    }
    return $products;
  }

  // Function to create an address
  public function createAddress($streetAddress, $extendedAddress, $city, $postalCode, $country, $region, $type){

    if(Auth::user()){
      $currentAddresses = Auth::user()->addresses;
      foreach ($currentAddresses as $key => $a) {
        if($type == 'billing' && $a->type == 'billing'){
          if($streetAddress != $a->streetAddress){
            $a->delete();
            $address = Address::create([
              'streetAddress' => $streetAddress,
              'extendedAddress' => $extendedAddress,
              'city' => $city,
              'postalCode' => $postalCode,
              'country' => $country,
              'region' => $region,
              'type' => $type,
              'user_id' => Auth::id()
            ]);
            return $address;
          }else{
            $address = $a;
            return $address;
          }
        }else if($type == 'shipping' && $a->type == 'shipping'){
          if($streetAddress != $a->streetAddress){
            $a->delete();
            $address = Address::create([
              'streetAddress' => $streetAddress,
              'extendedAddress' => $extendedAddress,
              'city' => $city,
              'postalCode' => $postalCode,
              'country' => $country,
              'region' => $region,
              'type' => $type,
              'user_id' => Auth::id()
            ]);
            return $address;
          }else{
            $address = $a;
            return $address;
          }
        }else{
          return 'same';
        }
      }
      if(count($currentAddresses) == 0){
        $address = Address::create([
          'streetAddress' => $streetAddress,
          'extendedAddress' => $extendedAddress,
          'city' => $city,
          'postalCode' => $postalCode,
          'country' => $country,
          'region' => $region,
          'type' => $type,
          'user_id' => Auth::id()
        ]);
        return $address;
      }
    }else{
      $address = Address::create([
        'streetAddress' => $streetAddress,
        'extendedAddress' => $extendedAddress,
        'city' => $city,
        'postalCode' => $postalCode,
        'country' => $country,
        'region' => $region,
        'type' => $type,
        'user_id' => Auth::id()
      ]);
      return $address;
    }

  }

}
