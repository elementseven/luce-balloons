<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Invoice;
use Auth;
use Hash;

class CustomerController extends Controller
{
    public function dashboard(){
        $billingaddress = Address::where([['user_id', Auth::id()], ['type', 'billing']])->first();
        $shippingaddress = Address::where([['user_id', Auth::id()], ['type', 'shipping']])->first();
        $invoices = Invoice::where('user_id', Auth::id())->orderBy('id', 'desc')->get();
        return view('dashboard')->with(['invoices' => $invoices, 'billingaddress' => $billingaddress, 'shippingaddress' => $shippingaddress]); 
    }

    public function account(){
        $billingaddress = Address::where([['user_id', Auth::id()], ['type', 'billing']])->first();
        $shippingaddress = Address::where([['user_id', Auth::id()], ['type', 'shipping']])->first();
        return view('customers.account')->with(['billingaddress' => $billingaddress, 'shippingaddress' => $shippingaddress]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        if($request->input('email') != $user->email){
            $this->validate($request,[
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'email' => 'required|string|email|max:255|unique:users'
            ]);
            $updated = array(   
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'email' => $request->input('email')
            );
            $user->update($updated);
        }else{
            $this->validate($request,[
                'first_name' => 'required|string',
                'last_name' => 'required|string'
            ]);
            $updated = array(   
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name')
            );
            $user->update($updated);
        }
        session(['action_status'=>'Success']);
        session(['action_message'=>'Details updated successfully.']);
        session(['action_colour'=>'text-success']);
        return back();
    }

    public function changePassword(Request $request){
        $user = Auth::user();
        $this->validate($request,[
            'current_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed'
        ]);
        $pwcheck = $this->check($user->password, $request->input('current_password'));
        if($pwcheck == true){
            $updated = array(
                'password' => bcrypt($request->input('password'))
            );
            $user->update($updated);
            session(['action_status'=>'Success']);
            session(['action_colour'=>'text-success']);
            session(['action_message'=>'Password changed successfully.']);
            return back();
        }
        session(['action_status'=>'Error']);
        session(['action_colour'=>'text-red']);
        session(['action_message'=>'The password you entered was incorrect.']);
        return back();
    }

    // Function to check password
    private function check($password, $check){
        if(Hash::check($check, $password)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Auth::user();
        $this->validate($request,[
            'current_password' => 'required|string'
        ]);
        $pwcheck = $this->check($user->password, $request->input('current_password'));
        if($pwcheck == true){
            $user->forceDelete();
        }else{
            session(['action_status'=>'Error']);
            session(['action_message'=>'The password you entered was incorrect.']);
        }
        return back();
    }

}
