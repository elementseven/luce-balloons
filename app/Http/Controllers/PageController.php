<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Post;
use App\Models\Blog;
use Auth;
use Mail;

class PageController extends Controller
{

  // Home page
  public function welcome(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('welcome', compact('posts'));
  }

  // Gallery page
  public function gallery(){
    return view('gallery');
  }

  // T&Cs page
  public function tandcs(){
    return view('tandcs');
  }

  // Privacy Policy page
  public function privacyPolicy(){
    return view('privacyPolicy');
  }

  // Custom Balloons page
  public function customBalloons(){
    return view('custom.index');
  }

  // Bubble Balloons page
  public function bubbleBalloons(){
    return view('custom.bubbleBalloons');
  }

  // Blog index page
  public function blog(){
    $categories = Blog::has('posts')->orderBy('title','asc')->get();
    return view('blog.index', compact('categories'));
  }

  // Return json of blogs
  public function getPosts(Request $request){
    if($request->input('category') == '*'){
      $posts = Post::where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
      ->orderBy('created_at','desc')
      ->with('blogs')
      ->paginate($request->input('limit'));
    }else{
      $posts = Post::whereHas('blogs', function($q) use($request){
          $q->where('id', $request->input('category'));
      })
      ->where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
      ->orderBy('created_at','desc')
      ->with('blogs')
      ->paginate($request->input('limit'));
    }

    // Return images with blog posts
    foreach($posts as $p){
      $p->featured = $p->getFirstMediaUrl('blog', 'featured');
      $p->featuredwebp = $p->getFirstMediaUrl('blog', 'featured-webp');
      $p->mimetype = $p->getFirstMedia('blog')->mime_type;
    }
    return $posts;
  }

  // Individual blog page
  public function blogShow($date, $slug){
    $post = Post::where('slug', $slug)->whereDate('created_at', $date)->where('status','published')->with('blogs')->first();
    $others = Post::where('id','!=', $post->id)->where('status', 'published')->orderBy('created_at','desc')->paginate(6);
    return view('blog.show',compact('post','others'));
  }

  // About page
  public function about(){
    return view('about');
  }

  // Contact page
  public function contact(){
    return view('contact');
  }

  // Services - Index
  public function services(){
    return view('services.index');
  }

  // Services - Parties
  public function parties(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('services.parties', compact('posts'));
  }
  // Services - Birthdays
  public function birthdays(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('services.birthdays', compact('posts'));
  }
  // Services - Baby Events
  public function babyEvents(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('services.babyEvents', compact('posts'));
  }
  // Services - Corporate
  public function corporate(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('services.corporate', compact('posts'));
  }
  // Services - Engagement
  public function engagement(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('services.engagement', compact('posts'));
  }
  // Services - Weddings
  public function weddings(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('services.weddings', compact('posts'));
  }

}
