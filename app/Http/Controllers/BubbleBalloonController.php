<?php

namespace App\Http\Controllers;

use App\Models\BubbleBalloon;
use App\Models\CustomOrder;
use Illuminate\Http\Request;
use Cart;

class BubbleBalloonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
          'balloon1' => 'required|string|max:255',
          'balloon2' => 'required|string|max:255',
          'textcolour' => 'required|string|max:255',
          'font' => 'required|string|max:255',
          'text' => 'required|string|max:255',
          'weight' => 'required|string|max:255',
          'size' => 'required',
          'price' => 'required',
          'total' => 'required',
          'quantity' => 'required'
        ]);

        $customorder = CustomOrder::create([
          'price' => $request->input('total')
        ]);

        $bubbleballoon = BubbleBalloon::create([
            'balloon1' => $request->input('balloon1'),
            'balloon2' => $request->input('balloon2'),
            'textcolour' => $request->input('textcolour'),
            'font' => $request->input('font'),
            'text' => $request->input('text'),
            'weight' => $request->input('weight'),
            'size' => $request->input('size'),
            'price' => $request->input('price'),
            'notes' => $request->input('notes'),
            'quantity' => $request->input('quantity'),
            'customorder_id' => $customorder->id
        ]);

        // Add the item to the cart
        Cart::add(['id' => $customorder->id, 'name' => 'Custom Bubble balloon', 'qty' => $bubbleballoon->quantity, 'price' => $bubbleballoon->price, 'weight' => 0, 'options' => ['image'=>'/img/customballoons/bubble.jpg', 'original_price'=> $bubbleballoon->price, 'sale_price'=> null, 'product_title' => 'Bubble balloon', 'product_excerpt' => 'Create your own bubble balloon!', 'inflated' => true, 'collectiondate' => null, 'shippingtype' => null, 'shipping' => 0.00]])->associate('App\Models\CustomOrder');

        return response()->json(['success' => 'success'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BubbleBalloon  $bubbleballoon
     * @return \Illuminate\Http\Response
     */
    public function show(BubbleBalloon $bubbleballoon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BubbleBalloon  $bubbleballoon
     * @return \Illuminate\Http\Response
     */
    public function edit(BubbleBalloon $bubbleballoon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BubbleBalloon  $bubbleballoon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BubbleBalloon $bubbleballoon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BubbleBalloon  $bubbleballoon
     * @return \Illuminate\Http\Response
     */
    public function destroy(BubbleBalloon $bubbleballoon)
    {
        //
    }
}
