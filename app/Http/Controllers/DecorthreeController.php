<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CustomOrder;
use App\Models\Decorthree;
use Cart;

class DecorthreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
          'balloon1' => 'required|string|max:255',
          'balloon2' => 'required|string|max:255',
          'balloon3' => 'required|string|max:255',
          'weight' => 'required|string|max:255',
          'price' => 'required',
          'total' => 'required',
          'quantity' => 'required'
        ]);

        $customorder = CustomOrder::create([
          'price' => $request->input('total')
        ]);

        $decorthree = Decorthree::create([
            'balloon1' => $request->input('balloon1'),
            'balloon2' => $request->input('balloon2'),
            'balloon3' => $request->input('balloon3'),
            'weight' => $request->input('weight'),
            'price' => $request->input('price'),
            'notes' => $request->input('notes'),
            'quantity' => $request->input('quantity'),
            'customorder_id' => $customorder->id
        ]);


        // Add the item to the cart
        Cart::add(['id' => $customorder->id, 'name' => 'Custom bunch of 3 balloons', 'qty' => $decorthree->quantity, 'price' => $decorthree->price, 'weight' => 0, 'options' => ['image'=>'/img/customballoons/3.jpg', 'original_price'=> $decorthree->price, 'sale_price'=> null, 'product_title' => 'Bunch of 3 balloons', 'product_excerpt' => 'Create your own bunch of 3 balloons!', 'inflated' => true, 'collectiondate' => null, 'shippingtype' => null, 'shipping' => 0.00]])->associate('App\Models\CustomOrder');

        return response()->json(['success' => 'success'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
