<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Decorthree extends Model
{
  use HasFactory;

  protected $fillable = [
    'balloon1','balloon2','balloon3','weight','price','quantity','notes','customorder_id'
  ];

  public function customorder(){
    return $this->belongsTo('App\Models\CustomOrder');
  }

}
