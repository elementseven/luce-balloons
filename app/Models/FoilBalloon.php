<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoilBalloon extends Model
{
    use HasFactory;

    protected $fillable = [
        'colour','shape','textcolour','font','text','extras','price','quantity','notes','customorder_id'
    ];

    public function customorder(){
        return $this->belongsTo('App\Models\CustomOrder');
    }

}
