<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class Product extends Model implements HasMedia
{
  use HasFactory;
  use InteractsWithMedia;
  
  protected $fillable = [
    'title','slug','status','excerpt','description','featured','slideshow_images'
  ];

  protected static function boot()
  {
      parent::boot();
      static::saving(function ($product) {
        $product->slug = Str::slug($product->title, "-");
      });
  }

  public function registerMediaConversions(Media $media = null): void
    {
  	$this->addMediaConversion('normal')->width(800);
    $this->addMediaConversion('normal-webp')->width(800)->format('webp');
    $this->addMediaConversion('square')->height(800)->crop('crop-center', 800, 800);
    $this->addMediaConversion('square-webp')->height(800)->crop('crop-center', 800, 800);
    $this->addMediaConversion('thumb')->crop('crop-center', 400, 400);
  }

  public function registerMediaCollections(): void
  {
      $this->addMediaCollection('featured')->singleFile();
      $this->addMediaCollection('slideshow_images');
  }

	public function variants(){
    return $this->hasMany('App\Models\Variant');
  }

	public function categories(){
    return $this->belongsToMany('App\Models\Category', 'category_product')->withPivot('category_id');
  }

  public function types(){
    return $this->belongsToMany('App\Models\Type', 'product_type')->withPivot('type_id');
  }

  public function invoices(){
    return $this->belongsToMany('App\Models\Invoice', 'invoice_product')->withPivot('invoice_id');
  }

  public function hasType($type) {

    return $this->types()
    ->where('type_id', $type)
    ->exists();
  }

}
