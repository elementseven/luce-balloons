<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BubbleBalloon extends Model
{
    use HasFactory;

    protected $fillable = [
        'balloon1','balloon2','textcolour','font','text','weight','size','price','quantity','notes','customorder_id'
    ];

    public function customorder(){
        return $this->belongsTo('App\Models\CustomOrder');
    }

}