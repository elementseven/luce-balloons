<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class Variant extends Model implements HasMedia
{
  use HasFactory;
  use InteractsWithMedia;

  protected $fillable = [
    'title','slug','status','description','featured','price','sale_price'
  ];

  protected static function boot()
  {
      parent::boot();
      static::saving(function ($product) {
        $product->slug = Str::slug($product->title, "-");
      });
  }

  public function registerMediaConversions(Media $media = null): void
    {
    $this->addMediaConversion('normal')->width(800);
    $this->addMediaConversion('normal-webp')->width(800)->format('webp');
    $this->addMediaConversion('thumb')->crop('crop-center', 400, 400);
  }

  public function registerMediaCollections(): void
  {
      $this->addMediaCollection('featured')->singleFile();
  }
  
  public function product(){
    return $this->belongsTo('App\Models\Product');
  }

  public function invoices(){
    return $this->belongsToMany('App\Models\Invoice', 'invoice_variant')->withPivot('invoice_id','paid','qty');
  }

}
