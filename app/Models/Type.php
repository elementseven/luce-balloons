<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Type extends Model
{
  use HasFactory;

  protected $fillable = [
    'title','slug'
  ];

  protected static function boot()
  {
    parent::boot();
    static::saving(function ($post) {
        $post->slug = Str::slug($post->title, "-");
    });
  }

  public function products(){
    return $this->belongsToMany('App\Models\Product', 'product_type')->withPivot('product_id');
  }

}
