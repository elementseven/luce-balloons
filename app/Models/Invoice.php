<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
  use HasFactory;

  protected $fillable = [
    'transaction_id','total','shipping','shipping_type', 'collection_date', 'event_date', 'notes','user_id'
  ];

  public function user(){
    return $this->belongsTo('App\Models\User');
  }

  public function products(){
    return $this->belongsToMany('App\Models\Product', 'invoice_product')->withPivot('invoice_id','qty');
  }

  public function variants(){
    return $this->belongsToMany('App\Models\Variant', 'invoice_variant')->withPivot('variant_id','paid','qty');
  }

  public function customorders(){
    return $this->hasMany('App\Models\CustomOrder');
  }

}