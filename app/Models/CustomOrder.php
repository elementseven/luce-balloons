<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'price','qty','user_id','invoice_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function invoice(){
        return $this->belongsTo('App\Models\Invoice');
    }

    public function decorthrees(){
        return $this->hasOne('App\Models\Decorthree','customorder_id');
    }

    public function decorfives(){
        return $this->hasOne('App\Models\Decorfive','customorder_id');
    }

    public function bubbleballoons(){
        return $this->hasOne('App\Models\BubbleBalloon','customorder_id');
    }

    public function foilballoons(){
        return $this->hasOne('App\Models\FoilBalloon','customorder_id');
    }

}
