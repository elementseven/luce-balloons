<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use ElementSeven\ProductInstructions\ProductInstructions;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Product::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Available'=>'Available', 'Unavailable'=>'Unavailable', 'Featured'=>'Featured');
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Title')->sortable()->rules('required'),
            Text::make('Excerpt')->onlyOnForms()->rules('required'),
            Select::make('Status')->options($statuses)->sortable()->rules('required'),
            Images::make('Featured image', 'featured')->conversionOnIndexView('thumb')->rules('required',"max:2000"),
            Images::make('Slideshow Images', 'slideshow_images')->conversionOnPreview('normal')->conversionOnDetailView('thumb')->conversionOnIndexView('thumb')->conversionOnForm('thumb')->fullSize()->singleImageRules("image", "max:2000")->onlyOnForms(),
            Trix::make('Description')->withFiles('public')->rules('required'),
            HasMany::make('Variants','variants'),
            BelongsToMany::make('Category','categories'),
            BelongsToMany::make('Type','types'),
            BelongsToMany::make('Invoices','invoices'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            (new ProductInstructions),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
