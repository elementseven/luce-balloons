<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/logout', function(){
	Auth::logout();
	return redirect()->to('/login');
})->name('logout');

// Pages
Route::get('/', [PageController::class, 'welcome'])->name('welcome');
Route::get('/about', [PageController::class, 'about'])->name('about');
Route::get('/contact', [PageController::class, 'contact'])->name('contact');
Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacy-policy');
Route::get('/terms-and-conditions', [PageController::class, 'tandcs'])->name('tandcs');

// Services pages
Route::get('/event-services', [PageController::class, 'services'])->name('event-services');
Route::get('/event-services/parties', [PageController::class, 'parties'])->name('services.parties');
Route::get('/event-services/birthdays', [PageController::class, 'birthdays'])->name('services.birthdays');
Route::get('/event-services/baby-events', [PageController::class, 'babyEvents'])->name('services.babyEvents');
Route::get('/event-services/corporate', [PageController::class, 'corporate'])->name('services.corporate');
Route::get('/event-services/engagement', [PageController::class, 'engagement'])->name('services.engagement');
Route::get('/event-services/weddings', [PageController::class, 'weddings'])->name('services.weddings');

// Blog routes
Route::get('/blog', [PageController::class, 'blog'])->name('blog');
Route::get('/blog/get', [PageController::class, 'getPosts'])->name('get-posts');
Route::get('/blog/{date}/{slug}', [PageController::class, 'blogShow'])->name('blog-single');

// Custom Balloons routes
Route::get('/custom-balloons', [PageController::class, 'customBalloons'])->name('custom-balloons');
Route::get('/bubble-balloons', [PageController::class, 'bubbleBalloons'])->name('bubble-balloons');
Route::post('/add-decorthree-to-basket', [DecorthreeController::class, 'create'])->name('adddecorthreetobasket');
Route::post('/add-decorfive-to-basket', [DecorfiveController::class, 'create'])->name('adddecorfivetobasket');
Route::post('/add-bubbleballoon-to-basket', [BubbleBalloonController::class, 'create'])->name('addbubbleballoontobasket');
Route::post('/add-foilballoon-to-basket', [FoilBalloonController::class, 'create'])->name('addfoilballoontobasket');

// Send enquiry form
Route::post('/send-message', [SendMail::class, 'enquiry'])->name('send-message');

// Shop Routes
Route::get('/shop', [ShopController::class, 'shop'])->name('shop');
Route::get('/shop/get', [ShopController::class, 'getProducts'])->name('get-products');
Route::get('/shop/get/popular', [ShopController::class, 'getPopularProducts'])->name('get-popular-products');
Route::get('/shop/{slug}', [ShopController::class, 'productShow'])->name('show-product');
Route::get('/shop/get/custom-balloons', [ShopController::class, 'getCustomBalloons'])->name('get-custom-balloon');
Route::get('/shop/bubble-balloons', function(){
	return redirect()->to('/bubble-balloons');
})->name('get-custom-balloon');

Route::get('/basket', [ShopController::class, 'basket'])->name('basket');
Route::get('/checkout', [ShopController::class, 'checkout'])->name('checkout');
Route::get('/add-to-basket/{product}/{variant}/{qty}', [ShopController::class, 'addToBasket'])->name('addtobasket');
Route::get('/shop/remove/{rowId}', [ShopController::class, 'remove'])->name('shop.removefrombasket');
Route::get('/empty-basket', [ShopController::class, 'emptyBasket'])->name('emptybasket');
Route::get('/shop/update-quantity/{rowId}/{qty}', [ShopController::class, 'updateQuantity'])->name('shop.updateQuantity');


// Checkout Routes
Route::post('/user/store', [ShopController::class, 'createUser'])->name('shop.create.user');
Route::get('/user/get-intent', [ShopController::class, 'getIntent'])->name('shop.user.getintent');
Route::post('/address/store', [ShopController::class, 'storeAddress'])->name('shop.address.store');
Route::post('/set-shipping', [ShopController::class, 'setShipping'])->name('shop.shipping.set');
Route::post('/payment/store', [ShopController::class, 'payment'])->name('shop.payment');
Route::get('/shop/success/{transaction_id}', [ShopController::class, 'success'])->name('success');

Route::get('/gallery', [PageController::class, 'gallery'])->name('gallery');
Route::get('/gallery/get', [ImageController::class, 'index'])->name('gallery.get');

// Login handler
Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'App\Http\Middleware\CustomerMiddleware'], function(){

	Route::get('/dashboard', [CustomerController::class, 'dashboard'])->name('customer-dashboard');

	Route::get('/dashboard/account', [CustomerController::class, 'account'])->name('account-management');
	Route::post('/update-account', [CustomerController::class, 'update'])->name('update-account');
	Route::post('/change-password', [CustomerController::class, 'changePassword'])->name('change-password');
	Route::post('/remove-account', [CustomerController::class, 'destroy'])->name('remove-account');


});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
