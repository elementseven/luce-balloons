(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["BubbleBalloons"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var Bubble = function Bubble() {
  return __webpack_require__.e(/*! import() | Bubble */ "Bubble").then(__webpack_require__.bind(null, /*! ./Bubble.vue */ "./resources/js/components/CustomBalloons/Bubble.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Bubble: Bubble
  },
  props: {
    fields: Object
  },
  data: function data() {
    return {
      balloons: [],
      errors: {},
      weight: 'Silver',
      weighthex: '#9b9b9b',
      textcolour: 'Black',
      textcolourhex: '#000000',
      font: 'Arial',
      text: 'Your text here',
      size: 20,
      max: 30,
      quantity: 1,
      notes: '',
      price: 24.99,
      total: 24.99,
      hastext: true,
      loaded: false
    };
  },
  mounted: function mounted() {
    for (var x = 0; x < 2; x++) {
      if (x == 0) {
        var balloon = {
          'type': 'Latex',
          'colour': "Pearl Light Blue",
          'hex': "#d6e3f1",
          'id': x
        };
      } else {
        var balloon = {
          'type': 'Latex',
          'colour': "Pearl Pink",
          'hex': "#e4cbd8",
          'id': x
        };
      }

      this.balloons.push(balloon);
    }

    console.log(this.balloons);
    this.loaded = true;
  },
  methods: {
    changeText: function changeText() {
      this.textcolourhex = document.getElementById('textcolour').querySelector(':checked').getAttribute('data-hex');
    },
    submit: function submit() {
      this.$emit('selected', {
        'balloons': this.balloons,
        'total': this.total,
        'quantity': this.quantity,
        'weight': this.weight,
        'notes': this.notes
      });
    },
    calculateTotal: function calculateTotal() {
      if (this.size == 20) {
        this.price = 24.99;
      } else {
        this.price = 29.99;
      }

      this.total = (this.price * this.quantity).toFixed(2);
    },
    changeBalloonColour: function changeBalloonColour(i, id) {
      this.balloons[i].hex = document.getElementById(id).querySelector(':checked').getAttribute('data-hex');
    },
    changeWeightColour: function changeWeightColour(i, id) {
      this.weighthex = document.getElementById('weightcolour').querySelector(':checked').getAttribute('data-hex');
    },
    confirm: function confirm(val) {
      this.$emit('selected', val);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=template&id=b9e2cfa2&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=template&id=b9e2cfa2& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "card p-5 mob-px-3 border-0 shadow" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c(
            "div",
            { staticClass: "col-lg-5 pr-5 mob-px-3 mob-mb-4" },
            [
              _vm.loaded == true
                ? _c("bubble", {
                    attrs: {
                      id: "custom-bubble",
                      balloons: _vm.balloons,
                      weighthex: _vm.weighthex,
                      font: _vm.font,
                      textcolourhex: _vm.textcolourhex,
                      text: _vm.text
                    }
                  })
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-7 mob-px-0 text-left" }, [
            _c("div", { staticClass: "row half_row" }, [
              _c("div", { staticClass: "col-lg-6 half_col text-left" }, [
                _vm._m(1),
                _vm._v(" "),
                _c("div", { staticClass: "select-holder mb-4" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.balloons[0].colour,
                          expression: "balloons[0].colour"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { id: "balloon-1-colour" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              _vm.balloons[0],
                              "colour",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          },
                          function($event) {
                            return _vm.changeBalloonColour(
                              0,
                              "balloon-1-colour"
                            )
                          }
                        ]
                      }
                    },
                    [
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#d6e3f1",
                            value: "Pearl Light Blue"
                          }
                        },
                        [_vm._v("Pearl Light Blue")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#ead936",
                            value: "Pearl Citrine Yellow"
                          }
                        },
                        [_vm._v("Pearl Citrine Yellow")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#d29844",
                            value: "Pearl Mandarin Orange"
                          }
                        },
                        [_vm._v("Pearl Mandarin Orange")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#bb4879",
                            value: "Pearl Magenta"
                          }
                        },
                        [_vm._v("Pearl Magenta")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#b24e4a",
                            value: "Pearl Ruby Red"
                          }
                        },
                        [_vm._v("Pearl Ruby Red")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#733445",
                            value: "Pearl Burgundy"
                          }
                        },
                        [_vm._v("Pearl Burgundy")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#352f6a",
                            value: "Pearl Quartz Purple"
                          }
                        },
                        [_vm._v("Pearl Quartz Purple")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#33649f",
                            value: "Pearl Sapphire Blue"
                          }
                        },
                        [_vm._v("Pearl Sapphire Blue")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#478388", value: "Pearl Teal" }
                        },
                        [_vm._v("Pearl Teal")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#243a55",
                            value: "Pearl Midnight Blue"
                          }
                        },
                        [_vm._v("Pearl Midnight Blue")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#8aad51",
                            value: "Pearl Lime Green"
                          }
                        },
                        [_vm._v("Pearl Lime Green")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#345e41",
                            value: "Pearl Forest Green"
                          }
                        },
                        [_vm._v("Pearl Forest Green")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#478768",
                            value: "Pearl Emerald Green"
                          }
                        },
                        [_vm._v("Pear Emerald Green")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#131510",
                            value: "Pearl Onyx Black"
                          }
                        },
                        [_vm._v("Pearl Onyx Black")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#f8f6f9", value: "Pearl White" }
                        },
                        [_vm._v("Pearl White")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#f1eed6", value: "Pearl Ivory" }
                        },
                        [_vm._v("Pearl Ivory")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#e7de81",
                            value: "Pearl Lemon Chiffon"
                          }
                        },
                        [_vm._v("Pearl Lemon Chiffon")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#e8d8d3", value: "Pearl Peach" }
                        },
                        [_vm._v("Pearl Peach")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#e4cbd8", value: "Pearl Pink" }
                        },
                        [_vm._v("Pearl Pink")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#bba0bf",
                            value: "Pearl Lavender"
                          }
                        },
                        [_vm._v("Pearl Lavender")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#96b9dc", value: "Pearl Azure" }
                        },
                        [_vm._v("Pearl Azure")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#d3deda",
                            value: "Pearl Mint Green"
                          }
                        },
                        [_vm._v("Pearl Mint Green")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#b0afaf",
                            value: "Metallic Silver"
                          }
                        },
                        [_vm._v("Metallic Silver")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#c9aa49",
                            value: "Metallic Gold"
                          }
                        },
                        [_vm._v("Metallic Gold")]
                      )
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6 half_col text-left" }, [
                _vm._m(2),
                _vm._v(" "),
                _c("div", { staticClass: "select-holder mb-4" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.balloons[1].colour,
                          expression: "balloons[1].colour"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { id: "balloon-2-colour" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              _vm.balloons[1],
                              "colour",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          },
                          function($event) {
                            return _vm.changeBalloonColour(
                              1,
                              "balloon-2-colour"
                            )
                          }
                        ]
                      }
                    },
                    [
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#d6e3f1",
                            value: "Pearl Light Blue"
                          }
                        },
                        [_vm._v("Pearl Light Blue")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#ead936",
                            value: "Pearl Citrine Yellow"
                          }
                        },
                        [_vm._v("Pearl Citrine Yellow")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#d29844",
                            value: "Pearl Mandarin Orange"
                          }
                        },
                        [_vm._v("Pearl Mandarin Orange")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#bb4879",
                            value: "Pearl Magenta"
                          }
                        },
                        [_vm._v("Pearl Magenta")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#b24e4a",
                            value: "Pearl Ruby Red"
                          }
                        },
                        [_vm._v("Pearl Ruby Red")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#733445",
                            value: "Pearl Burgundy"
                          }
                        },
                        [_vm._v("Pearl Burgundy")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#352f6a",
                            value: "Pearl Quartz Purple"
                          }
                        },
                        [_vm._v("Pearl Quartz Purple")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#33649f",
                            value: "Pearl Sapphire Blue"
                          }
                        },
                        [_vm._v("Pearl Sapphire Blue")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#478388", value: "Pearl Teal" }
                        },
                        [_vm._v("Pearl Teal")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#243a55",
                            value: "Pearl Midnight Blue"
                          }
                        },
                        [_vm._v("Pearl Midnight Blue")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#8aad51",
                            value: "Pearl Lime Green"
                          }
                        },
                        [_vm._v("Pearl Lime Green")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#345e41",
                            value: "Pearl Forest Green"
                          }
                        },
                        [_vm._v("Pearl Forest Green")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#478768",
                            value: "Pearl Emerald Green"
                          }
                        },
                        [_vm._v("Pear Emerald Green")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#131510",
                            value: "Pearl Onyx Black"
                          }
                        },
                        [_vm._v("Pearl Onyx Black")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#f8f6f9", value: "Pearl White" }
                        },
                        [_vm._v("Pearl White")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#f1eed6", value: "Pearl Ivory" }
                        },
                        [_vm._v("Pearl Ivory")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#e7de81",
                            value: "Pearl Lemon Chiffon"
                          }
                        },
                        [_vm._v("Pearl Lemon Chiffon")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#e8d8d3", value: "Pearl Peach" }
                        },
                        [_vm._v("Pearl Peach")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#e4cbd8", value: "Pearl Pink" }
                        },
                        [_vm._v("Pearl Pink")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#bba0bf",
                            value: "Pearl Lavender"
                          }
                        },
                        [_vm._v("Pearl Lavender")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: { "data-hex": "#96b9dc", value: "Pearl Azure" }
                        },
                        [_vm._v("Pearl Azure")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#d3deda",
                            value: "Pearl Mint Green"
                          }
                        },
                        [_vm._v("Pearl Mint Green")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#b0afaf",
                            value: "Metallic Silver"
                          }
                        },
                        [_vm._v("Metallic Silver")]
                      ),
                      _vm._v(" "),
                      _c(
                        "option",
                        {
                          attrs: {
                            "data-hex": "#c9aa49",
                            value: "Metallic Gold"
                          }
                        },
                        [_vm._v("Metallic Gold")]
                      )
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _vm.hastext
                ? _c("div", { staticClass: "col-6 half_col mb-3" }, [
                    _vm._m(3),
                    _vm._v(" "),
                    _c("div", { staticClass: "select-holder" }, [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.textcolour,
                              expression: "textcolour"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "textcolour" },
                          on: {
                            change: [
                              function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.textcolour = $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              },
                              function($event) {
                                return _vm.changeText()
                              }
                            ]
                          }
                        },
                        [
                          _c(
                            "option",
                            {
                              attrs: {
                                "data-hex": "#fff",
                                value: "White",
                                checked: ""
                              }
                            },
                            [_vm._v("White")]
                          ),
                          _vm._v(" "),
                          _c(
                            "option",
                            { attrs: { "data-hex": "#000", value: "Black" } },
                            [_vm._v("Black")]
                          ),
                          _vm._v(" "),
                          _c(
                            "option",
                            { attrs: { "data-hex": "#666", value: "Silver" } },
                            [_vm._v("Silver")]
                          ),
                          _vm._v(" "),
                          _c(
                            "option",
                            { attrs: { "data-hex": "#c9aa49", value: "Grey" } },
                            [_vm._v("Gold")]
                          )
                        ]
                      )
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.hastext
                ? _c("div", { staticClass: "col-6 half_col mb-3" }, [
                    _vm._m(4),
                    _vm._v(" "),
                    _c("div", { staticClass: "select-holder" }, [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.font,
                              expression: "font"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "custom_balloon_font_" + (_vm.i - 1) },
                          on: {
                            change: [
                              function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.font = $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              },
                              function($event) {
                                return _vm.changeText()
                              }
                            ]
                          }
                        },
                        [
                          _c("option", { attrs: { value: "Arial" } }, [
                            _vm._v("Arial")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "Lora" } }, [
                            _vm._v("Lora")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "Montserrat" } }, [
                            _vm._v("Montserrat")
                          ])
                        ]
                      )
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.hastext
                ? _c("div", { staticClass: "col-12 half_col mb-3" }, [
                    _vm._m(5),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.text,
                            expression: "text"
                          }
                        ],
                        staticClass: "form-control border",
                        attrs: {
                          id: "custom_balloon_text_" + (_vm.i - 1),
                          placeholder: "Balloon Text",
                          maxlength: _vm.max
                        },
                        domProps: { value: _vm.text },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.text = $event.target.value
                          }
                        }
                      })
                    ])
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row half_row" }, [
              _c("div", { staticClass: "col-6 half_col" }, [
                _vm._m(6),
                _vm._v(" "),
                _c("div", { staticClass: "select-holder" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.size,
                          expression: "size"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { id: "size" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.size = $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          },
                          _vm.calculateTotal
                        ]
                      }
                    },
                    [
                      _c("option", { attrs: { value: "20" } }, [
                        _vm._v("20 Inch")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "24" } }, [
                        _vm._v("24 Inch")
                      ])
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-6 half_col" }, [
                _vm._m(7),
                _vm._v(" "),
                _c("div", { staticClass: "select-holder" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.quantity,
                          expression: "quantity"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { id: "quantity" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.quantity = $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          },
                          _vm.calculateTotal
                        ]
                      }
                    },
                    [
                      _c("option", { attrs: { value: "1" } }, [_vm._v("1")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "2" } }, [_vm._v("2")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "3" } }, [_vm._v("3")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "4" } }, [_vm._v("4")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "5" } }, [_vm._v("5")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "6" } }, [_vm._v("6")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "7" } }, [_vm._v("7")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "8" } }, [_vm._v("8")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "9" } }, [_vm._v("9")])
                    ]
                  )
                ]),
                _vm._v(" "),
                _vm._m(8)
              ]),
              _vm._v(" "),
              _vm._m(9)
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("hr", { staticClass: "my-4" }),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _vm._m(10),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.notes,
                        expression: "notes"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      id: "notes",
                      rows: "5",
                      placeholder:
                        "Tell us anything else we might need to know about your custom order"
                    },
                    domProps: { value: _vm.notes },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.notes = $event.target.value
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-12 half_col text-left" }, [
                _vm._m(11),
                _vm._v(" "),
                _c(
                  "p",
                  { staticClass: "text-larger mb-1 text-center text-lg-right" },
                  [
                    _c("b", [_vm._v("Total:")]),
                    _vm._v(" £" + _vm._s(_vm.total) + " "),
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-red ml-4",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("Add to basket")]
                    )
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-12 mb-5" }, [
        _c("h1", { staticClass: "mt-5 mb-2 text-primary" }, [
          _vm._v("Bubble Balloons")
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "Create your own custom bubble balloon with our bubble balloon builder! These balloons can be further customised after purchase, just let us know what you need in the form below. To find out more about our bubble balloons, check out our "
          ),
          _c("a", { attrs: { href: "/bubble-balloons", target: "_blank" } }, [
            _vm._v("bubble balloons page!")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "pl-3 mb-2" }, [
      _c("b", [_vm._v("Inner Balloon colour 1")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "pl-3 mb-2" }, [
      _c("b", [_vm._v("Inner Balloon colour 2")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "pl-3" }, [
      _c("b", [_vm._v("Text Colour")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "pl-3" }, [_c("b", [_vm._v("Font")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "pl-3" }, [
      _c("b", [_vm._v("Text (max 30 characters)")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "pl-3 mb-1" }, [
      _c("b", [_vm._v("Balloon Size")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "pl-3 mb-1" }, [
      _c("b", [_vm._v("Quantity*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "text-small ml-3 mb-0 d-none d-lg-block" }, [
      _vm._v("*Please "),
      _c("a", { attrs: { href: "/contact", target: "_blank" } }, [
        _vm._v("get in touch")
      ]),
      _vm._v(" to order over 9 units")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12" }, [
      _c("p", { staticClass: "text-small ml-3 mb-0 d-lg-none" }, [
        _vm._v("*Please "),
        _c("a", { attrs: { href: "/contact", target: "_blank" } }, [
          _vm._v("get in touch")
        ]),
        _vm._v(" to order over 9 units")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "ml-3", attrs: { for: "notes" } }, [
      _c("b", [_vm._v("Any additional information about your order")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-check mb-4" }, [
      _c("input", {
        staticClass: "form-check-input",
        attrs: { type: "checkbox", value: "", id: "accept", required: "" }
      }),
      _vm._v(" "),
      _c(
        "label",
        { staticClass: "form-check-label", attrs: { for: "accept" } },
        [
          _vm._v(
            "\n\t\t\t\t\t\t        I understand that my balloons will not look identical to the graphics shown and the final product may differ in appearance.\n\t\t\t\t\t\t      "
          )
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/CustomBalloons/BubbleBalloons.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/components/CustomBalloons/BubbleBalloons.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BubbleBalloons_vue_vue_type_template_id_b9e2cfa2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BubbleBalloons.vue?vue&type=template&id=b9e2cfa2& */ "./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=template&id=b9e2cfa2&");
/* harmony import */ var _BubbleBalloons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BubbleBalloons.vue?vue&type=script&lang=js& */ "./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BubbleBalloons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BubbleBalloons_vue_vue_type_template_id_b9e2cfa2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BubbleBalloons_vue_vue_type_template_id_b9e2cfa2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/CustomBalloons/BubbleBalloons.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BubbleBalloons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BubbleBalloons.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BubbleBalloons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=template&id=b9e2cfa2&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=template&id=b9e2cfa2& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BubbleBalloons_vue_vue_type_template_id_b9e2cfa2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BubbleBalloons.vue?vue&type=template&id=b9e2cfa2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CustomBalloons/BubbleBalloons.vue?vue&type=template&id=b9e2cfa2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BubbleBalloons_vue_vue_type_template_id_b9e2cfa2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BubbleBalloons_vue_vue_type_template_id_b9e2cfa2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);